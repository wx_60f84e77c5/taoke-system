<?php

namespace app\index\controller;

use think\Controller;

class Share extends Controller
{
    /**
     * 淘宝分享页
     */
    public function tb()
    {
        $id = input('id');
        $info = model('Tkl')->get($id);
        if (!$info){
            exit();
        }else {
            $info['pics'] = json_decode($info['pics'],true);
            
            $this->assign('info',$info);
        }
        return $this->fetch();
    }
}
