<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\common\controller;

use think\Controller;
/**
 * 短信发送类
 * @author zqs
 */
class Sms extends Controller
{
    /**
     * 注册登录短信发送
     */
    public function reglogin($mobile, $code)
    {
        return send_sms($mobile, $code,'reglogin');
    }
    /**
     * 绑定手机
     */
    public function bindmobile($mobile, $code)
    {
        return send_sms($mobile, $code,'bindmobile','alisms_tpl_identity');
    }
    /**
     * 绑定支付宝
     */
    public function bindalipay($mobile, $code)
    {
        return send_sms($mobile, $code,'bindalipay','alisms_tpl_identity');
    }
    /**
     * 邀请页接码
     */
    public function invite($mobile, $code)
    {
        return send_sms($mobile, $code,'invite');
    }
}
