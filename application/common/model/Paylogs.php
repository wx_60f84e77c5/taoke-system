<?php

namespace app\common\model;

use think\Model;

class Paylogs extends Model
{
    public function getUserNicknameAttr($value,$data){
        $nickname = get_nickname($data['uid']);
        if (!$nickname){
            //$nickname = '无用户';
        }
        return $nickname;
    }
    //浏览类型
    public function getPayTypeTextAttr($value,$data)
    {
        $text = ['支付宝','微信','支付宝h5'];
        return $text[$data['pay_type']];
    }
    //0待支付1成功2退款
    public function getPayStatusTextAttr($value,$data)
    {
        $text = ['待支付','支付成功','退款'];
        return $text[$data['pay_status']];
    }
    //购买等级
    public function getLevalTextAttr($value,$data)
    {
        $text = config('site.user_leval');
        return $text[$data['leval']];
    }
}
