<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\admin\controller\user;
use app\common\controller\Admin;

class Index extends Admin
{
    protected $model = null;
    public function initialize()
    {
        parent::initialize();
        $this->model = model('User');
    }
    //会员列表
    public function index()
    {
        if ($this->request->isAjax())
        {
            //状态
            $status = input('status',-1);
            if ($status!=-1){
                $map[] = ['status','eq',$status];
            }else {
                $map[] = ['status','neq',-1];
            }
            //级别
            $leval = input('leval');
            if (!empty($leval)){
                $map[] = ['leval','eq',$leval];
            }
            
            //智能识别关键词
            $keyword = input('keyword');
            if (!empty($keyword)){
                if (is_numeric($keyword) && strlen($keyword)==11){
                    $map[] = ['mobile','eq',$keyword];
                }elseif (is_numeric($keyword) && strlen($keyword)==6){
                    $map[] = ['uid','eq',$keyword];
                }elseif (strlen($keyword)==8){
                    $map[] = ['invite_code','eq',$keyword];
                }else {
                    $map[] = ['nickname','eq',$keyword];
                }
            }else {
            	//时间条件
            	$data = input('date');
            	if ($data){
            		list($date_start,$date_end) = str2arr($data,' ~ ');
            		$map[] = ['create_time','between time',[$date_start,$date_end]];
            	}
            }
            //排序
            $order = input('order','uid desc');
            
            $list = $this->model
            ->where($map)
            ->order($order)
            ->paginate(input('limit',15));
            $list->append(['status_text','pid_nickname']);
            //追加上上级
            foreach ($list as $k=>$v){
                $list[$k]['ppid'] = db('user')->where('uid',$v['pid'])->value('pid');
                $list[$k]['ppname'] = get_nickname((int)$list[$k]['ppid']); 
            }
            //返回layui分页
            return json(layui_page($list));
        }
        check_leval();//检测会员过期
        $leval_arr = get_db_config(true)['user_leval_cfg']['name']??config('site.user_leval');
        $this->assign('leval_arr',$leval_arr);
        return $this->fetch();
    }
    /**
     * 编辑
     */
    public function edit()
    {
        $ids = input('ids');
        if (empty($ids)) {
            $this->error('无效数据');
        }
        $uinfo = $this->model->get($ids);
        if (!$uinfo){
            $this->error('无数据');
        }
        if ($this->request->isAjax())
        {
            $uinfo->leval = input('leval');
            $uinfo->pid = input('pid');
            $uinfo->experience = input('experience');
            $uinfo->score = input('score');
            $uinfo->money = input('money');
            $uinfo->status = input('status');
            if ($uinfo->save()){
                $this->success('编辑成功');
            }else {
                $this->error('编辑失败');
            }
        }
        $leval_arr = get_db_config(true)['user_leval_cfg']['name']??config('site.user_leval');
        $this->assign('leval_arr',$leval_arr);
        $this->assign('uinfo',$uinfo);//dump($uinfo);
        return $this->fetch();
        
    }
    /**
     * 删除
     */
    public function del($ids='')
    {
        if ($ids) {
            $data = [
                'delete_time' => time(),
                'status' => -1,
                'delete_uid' => $this->auth->uid,
            ];
            $count = $this->model->allowField(true)->where('uid','in',$ids)->update($data);
            if ($count){
                $this->success('删除成功');
            }else{
                $this->success('删除失败');
            }
        }else{
            $this->error('请选择您要操作的数据');
        }
        
        return;
    }
    /**
     * 冻结
     */
    public function frozen($ids='')
    {
        if ($ids) {
            $count = $this->model->where('uid','in',$ids)->setField('status',0);
            if ($count){
                $this->success('冻结成功');
            }else{
                $this->success('冻结失败');
            }
        }else{
            $this->error('请选择您要操作的数据');
        }
        
        return;
    }
    /**
     * 用户信息
     */
    public function info()
    {
        $uid = (int)input('id');
        if (empty($uid)) return json('参数错误');
        if ($this->request->isAjax()){
            $type = input('type',0);
            if ($type>=0 && $type<4){
                if ($type==0){//资金记录
                    $list = model('MoneyLogs')
                    ->where('uid',$uid)
                    ->order('id desc')
                    ->paginate(input('limit',15));
                    //$list->append(['status_text','pid_nickname']);
                    //返回layui分页
                    return json(layui_page($list));
                }else if ($type==1){//推荐记录
                    $map = [['pid','eq',$uid]];
                    $ds = intval(input('ds'));
                    if ($ds>1){
                        $ids = $this->model->GetDesignSon($uid,$ds);
                        $map = [['uid','in',$ids]];
                    }
                    $list = $this->model
                    ->where($map)
                    ->order('uid desc')
                    ->paginate(input('limit',15));
                    $list->append(['status_text']);
                    //返回layui分页
                    return json(layui_page($list));
                }else if ($type==2){//提现记录
                    $list = model('Draw')
                    ->where('uid',$uid)
                    ->order('id desc')
                    ->paginate(input('limit',15));
                    $list->append(['status_text']);
                    return json(layui_page($list));
                }else if ($type==3){//积分记录
                    $list = model('ScoreLogs')
                    ->where('uid',$uid)
                    ->order('id desc')
                    ->paginate(input('limit',15));
                    //$list->append(['type_text']);
                    return json(layui_page($list));
                }
            }else {
                $this->error('参数错误');
            }
        }else {
            $uid = (int)input('id');
            if (empty($uid)) return json('参数错误');
            $user = $this->model->get($uid);
            if (!$user) return json('用户信息不存在');
            
            $this->assign('info',$user);
            return $this->fetch();
        }
        
    }
    /**
     * 浏览记录
     */
    public function goods_logs()
    {
        if ($this->request->isAjax())
        {
            $map = [];
            $list = model('GoodsLogs')
            ->where($map)
            ->order('id desc')
            ->paginate(input('limit',15));
            $list->append(['user_nickname','view_type_text','is_collect_text','is_tmall_text']);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }
    /**
     * 支付记录
     */
    public function pay_logs()
    {
        if ($this->request->isAjax())
        {
            //待审核
            $status = input('pay_status');
            if (isset($status) && $status!=''){
                $map[] = ['pay_status','=',$status];
            }else{
                $map = [
                    ['pay_status','eq',1]
                ];
            }
            //智能识别关键词
            $keyword = input('keyword');
            if (!empty($keyword)){
                $map[] = ['uid','eq',$keyword];
            }
            //$map[]=['pay_type','=',0];
            $list = model('Paylogs')
            ->where($map)
            ->order('id desc')
            ->paginate(input('limit',15));
            $list->append(['user_nickname','pay_type_text','pay_status_text','leval_text']);
            //dump($list);
            //返回layui分页
            return json(layui_page($list));
        }
        return $this->fetch();
    }
    
    /**
     * 导出会员列表、提现记录、支付记录
     */
    public function export()
    {
    	$params = input();
    	$ids = $params['ids'] ?? [];
    	$type = empty($params['type'])?'user':$params['type'];
    	$map = [];
    	//无选中
    	if (empty($ids)){
    		//日期
    		if ($params['date']){
    			list($date_start,$date_end) = str2arr($params['date'],' ~ ');
    		}else{
    			$date_start = date('Y-m-d 00:00:00',time());
    			$date_end = date('Y-m-d 23:59:59',time());
    		}
    		$map[] = ['create_time','between time',[$date_start,$date_end]];
    	}else {
    		if ($type=='user'){
    			$map[] = ['id','in',$ids];
    		}else {
    			$map[] = ['uid','in',$ids];
    		}
    	}
    	switch ($type){
    		case 'user':
    			//等级
    			if ($params['leval']){
    				$map[] = ['leval','=',$params['leval']];
    			}
    			//状态
    			if ($params['status']=='-1'){
    				$map[] = ['status','neq',-1];
    			}else {
    				$map[] = ['status','=',$params['status']];
    			}
    			//关键词
    			if ($params['keyword']){
    				//智能识别关键词
    				$keyword = $params['keyword'];
    				if (!empty($keyword)){
    					if (is_numeric($keyword) && strlen($keyword)==11){
    						$map[] = ['mobile','eq',$keyword];
    					}elseif (is_numeric($keyword) && strlen($keyword)==6){
    						$map[] = ['uid','eq',$keyword];
    					}elseif (strlen($keyword)==8){
    						$map[] = ['invite_code','eq',$keyword];
    					}else {
    						$map[] = ['nickname','eq',$keyword];
    					}
    				}
    			}
    			$list = $this->model->where($map)->field('uid,nickname,pid,mobile,money,score,leval,create_time,invite_code')->select();
    			$headArr = ['ID','昵称','推荐人','手机','余额','积分','会员等级','注册时间','邀请码'];
    			//设置一些宽度
    			$width = [
    				'B' => 20,
    				'D' => 15,
    				'G' => 15,
    				'H' => 20,
    				'I' => 15
    			];
    			set_time_limit(0);
    			ini_set("memory_limit", "2048M");
    			export_excel('会员列表', $headArr, $list->toArray(),$width);
    			break;
    		case 'pay_logs':
    			//支付记录
    			$status = $params['pay_status'];
    			if (isset($status) && $status!=''){
    				$map[] = ['pay_status','=',$status];
    			}else{
    				$map[] = ['pay_status','=',1];
    			}
    			//智能识别关键词
    			$keyword = $params['keyword'];
    			if (!empty($keyword)){
    				$map[] = ['uid','eq',$keyword];
    			}
    			$list = model('Paylogs')->where($map)->field('uid,amount,create_time,out_trade_no,trade_no,pay_status')->select();
    			$status_arr = ['未支付','已支付'];
    			foreach ($list as $k=>$v){
    				$list[$k]['pay_status'] = $status_arr[$v['pay_status']];
    			}
    			$headArr = ['用户ID','支付金额','购买项目时间','本地单号','第三方单号','状态'];
    			//设置一些宽度
    			$width = [
    				'C' => 20,
    				'D' => 35,
    				'E' => 35
    			];
    			set_time_limit(0);
    			ini_set("memory_limit", "2048M");
    			export_excel('支付记录', $headArr, $list->toArray(),$width);
    			break;
    	}
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
