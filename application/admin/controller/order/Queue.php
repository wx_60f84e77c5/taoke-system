<?php

namespace app\admin\controller\order;

use think\Controller;

class Queue extends Controller
{
    /**
     * 任务队列
     */
    public function index()
    {
        $this->redirect('/phpRedisAdmin');
    }
}
