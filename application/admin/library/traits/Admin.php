<?php

namespace app\admin\library\traits;

trait Admin
{

    /**
     * 查看
     */
    public function index()
    {
        if ($this->request->isAjax())
        {
            list($where, $sort, $order, $offset, $limit) = $this->buildparams();
            /* $total = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->count();

            $list = $this->model
                    ->where($where)
                    ->order($sort, $order)
                    ->limit($offset, $limit)
                    ->select();
            $result = array("total" => $total, "rows" => $list);

            return json($result); */
            $list = $this->model
            ->where('status','neq',-1)
            ->where($where)
            ->order($sort, $order)
            ->paginate(input('limit',15));
            //数据转换
            $list = [
            		'code' => 0,
            		'msg'  => '',
            		'data' => $list->toArray()['data'],
            		'count'=> $list->toArray()['total'],
            ];
            return json($list);
        }
        return $this->view->fetch();
    }

    /**
     * 添加
     */
    public function add()
    {
        if ($this->request->isPost())
        {
            $this->code = -1;
            $this->msg = '添加失败';
            $params = $this->request->post("row/a");
            if ($params)
            {
                foreach ($params as $k => &$v)
                {
                    $v = is_array($v) ? implode(',', $v) : $v;
                    $v = substr($k, -4) == 'time' && !is_numeric($v) ? strtotime($v) : $v;
                }
                $this->model->create($params);
                $this->code = 1;
                $this->msg = '添加成功';
            }

            return;
        }
        return $this->view->fetch();
    }

    /**
     * 编辑
     */
    public function edit($ids = NULL)
    {
        $row = $this->model->get($ids);
        if (!$row)
            $this->error('No Results were found');
        if ($this->request->isPost())
        {
            $this->code = -1;
            $this->msg = '编辑失败';
            $params = $this->request->post("row/a");
            if ($params)
            {
                foreach ($params as $k => &$v)
                {
                    $v = is_array($v) ? implode(',', $v) : $v;
                    $v = substr($k, -4) == 'time' && !is_numeric($v) ? strtotime($v) : $v;
                }
                $row->save($params);
                $this->code = 1;
                $this->msg = '编辑成功';
            }

            return;
        }
        $this->view->assign("row", $row);
        return $this->view->fetch('add');
    }

    /**
     * 删除
     */
    public function del($ids = "")
    {
        $this->code = -1;
        $this->msg = '删除失败';
        if ($ids)
        {
            $data = [
              'delete_time' => time(),
              'status' => -1,
              'delete_uid' => $this->auth->uid,
            ];
            $count = $this->model->allowField(true)->save($data,['id'=>['in',$ids]]);
            if ($count)
            {
                $this->code = 1;
                $this->msg = '删除成功';
            }
        }
        else 
        {
            $this->msg = '请选择您要操作的数据';
        }

        return;
    }

    /**
     * 批量更新
     */
    public function multi($ids = "")
    {
        $this->code = -1;return ;
        $ids = $ids ? $ids : $this->request->param("ids");
        if ($ids)
        {
            if ($this->request->has('params'))
            {
                parse_str($this->request->post("params"), $values);
                $values = array_intersect_key($values, array_flip(array('status')));
                if ($values)
                {
                    $count = $this->model->where($this->model->getPk(), 'in', $ids)->update($values);
                    if ($count)
                    {
                        $this->code = 1;
                    }
                }
            }
            else
            {
                $this->code = 1;
            }
        }

        return;
    }
    /**
     * 生成查询所需要的条件,排序方式
     * @param mixed $searchfields 查询条件
     * @return array
     */
    protected function buildparams($searchfields = NULL)
    {
        $searchfields = is_null($searchfields) ? 'id' : $searchfields;
        $search = $this->request->get("search", '');
        $filter = $this->request->get("filter", '');
        $op = $this->request->get("op", '');
        $sort = $this->request->get("sort", "id");
        $order = $this->request->get("order", "DESC");
        $offset = $this->request->get("offset", 0);
        $limit = $this->request->get("limit", 0);
        $filter = json_decode($filter, TRUE);
        $op = json_decode($op, TRUE);
        $filter = $filter ? $filter : [];
        $where = [];
        if ($search)
        {
            $searcharr = is_array($searchfields) ? $searchfields : explode(',', $searchfields);
            $searchlist = [];
            foreach ($searcharr as $k => $v)
            {
                $searchlist[] = "`{$v}` LIKE '%{$search}%'";
            }
            $where[] = "(" . implode(' OR ', $searchlist) . ")";
        }
        $table = '';
        if (!empty($this->model))
        {
            $class = get_class($this->model);
            $name = basename(str_replace('\\', '/', $class));
            $table = $this->model->db(false)->getTable($name);
            $table = $table . ".";
        }
        if (stripos($sort, ".") === false)
        {
    
            $sort = $table . $sort;
        }
        foreach ($filter as $k => $v)
        {
            $sym = isset($op[$k]) ? $op[$k] : '=';
            if (stripos($k, ".") === false)
            {
                $k = $table . $k;
            }
            $sym = isset($op[$k]) ? $op[$k] : $sym;
            switch ($sym)
            {
                case '=':
                case '!=':
                case 'LIKE':
                case 'NOT LIKE':
                    $where[] = [$k, $sym, $v];
                    break;
                case '>':
                case '>=':
                case '<':
                case '<=':
                    $where[] = [$k, $sym, intval($v)];
                    break;
                case 'IN(...)':
                case 'NOT IN(...)':
                    $where[] = [$k, str_replace('(...)', '', $sym), explode(',', $v)];
                    break;
                case 'BETWEEN':
                case 'NOT BETWEEN':
                    $where[] = [$k, $sym, array_slice(explode(',', $v), 0, 2)];
                    break;
                case 'LIKE %...%':
                    $where[] = [$k, 'LIKE', "%{$v}%"];
                    break;
                case 'IS NULL':
                case 'IS NOT NULL':
                    $where[] = [$k, strtolower(str_replace('IS ', '', $sym))];
                    break;
                default:
                    break;
            }
        }
        $where = function($query) use ($where) {
            foreach ($where as $k => $v)
            {
                if (is_array($v))
                {
                    call_user_func_array([$query, 'where'], $v);
                }
                else
                {
                    $query->where($v);
                }
            }
        };
        return [$where, $sort, $order, $offset, $limit];
    }

}
