<?php

namespace app\api\controller;

use think\Controller;
/**
 * 商品详情，分享生成等
 * @author zqs
 *
 */
class Goods extends User
{
    /**
     * 商品详情-待改造,查询商品信息-并查出券信息
     */
    public function item_info()
    {
        $tjp = input('tjp','t');
        $item_id = input('item_id');
        $re = false;
        switch ($tjp){
            case 't':
                $re = $this->taobao_item_info($item_id);
                break;
            case 'j':
                $re = $this->jd_item_info($item_id,input('url'));
                break;
            case 'p':
                $re = $this->pdd_item_info($item_id);
                break;
            default:
                $re = $this->taobao_item_info($item_id);
        }
        if ($re){
            $re['earn_share'] = $re['earn_zigou'] = 0;
            //分享赚
            $leval = $this->uinfo->getData('leval');
            $db_config = get_db_config(true);
            $lvcfg = $db_config['user_leval_cfg'];
            $share_cfg = $lvcfg['share'];
            $zi_cfg = $lvcfg['zigou'];
            //if ($leval>0){
                $re['earn_share'] = round($re['price_after_quan']*$re['rate']*$share_cfg[$leval]/10000,2);
                $re['earn_zigou'] = round($re['price_after_quan']*$re['rate']*$zi_cfg[$leval]/10000,2);
            //}
            
            $re['is_collect'] = 0;
            //是否已经收藏
            if (model('GoodsLogs')->get(['uid'=>$this->uinfo['uid'],'item_id'=>$item_id,'tjp'=>$tjp,'is_collect'=>1])){
                $re['is_collect'] = 1;
            }
            //浏览与收藏、取消收藏
            $is_collect = input('collect',0);
            if ($is_collect==2){
                //取消收藏 
                model('GoodsLogs')->where(['uid'=>$this->uinfo['uid'],'item_id'=>$item_id,'tjp'=>$tjp,'is_collect'=>1])->delete();
            }else {
                $goodlog = model('GoodsLogs')->get(['uid'=>$this->uinfo['uid'],'item_id'=>$item_id,'tjp'=>$tjp,'is_collect'=>$is_collect]);
                if ($goodlog){
                    $goodlog->view_count = $goodlog->view_count+1;
                    $goodlog->save();
                    
                }else {
                    $data = [
                        'uid' => $this->uid,
                        'nickname' => $this->uinfo['nickname'],
                        'title' => $re['title']??'',
                        'item_id' => $item_id,
                        'couponurl' => input('url',''),
                        'pic' => $re['pic'],
                        'view_type' => input('search',0),
                        'tjp' => $tjp,
                        'is_collect' => $is_collect,
                        'price' => $re['price'],
                        'price_after_quan' => $re['price_after_quan'],
                        'is_tmall' => $re['is_tmall']
                    ];
                    model('GoodsLogs')->allowField(true)->save($data);
                }
            }
            //监听浏览任务
            add_task_score(7, $this->uid);
            $this->success('获取成功','',$re);
        }else {
            $this->error('获取失败');
        }
            
    }
    /**
     * +猜你喜欢
     * 这个只有淘宝支持 后续再说
     */
    public function relation_goods(){}
    /**
     * 淘宝-商品详情
     */
    public function taobao_item_info($item_id)
    {
        if (empty($item_id)) $this->error('id can`t be null');
        $cache_data = cache('taobao_item_info_'.$item_id);
        if ($cache_data){
            return $cache_data;
        }
        //高佣转链
        $tk = new \ddj\Tk();
        $info = $tk->gaoyong($item_id);//dump($info);die;
        if (!$info){
            return false;
        }
        $return = [
            'item_id' => $item_id,
            'title' => $info['item']['title'],
            'price' => $info['item']['discountPrice'],
            'quan_price' => 0,
            'price_after_quan' => $info['item']['discountPrice'],
            'rate' => $info['max_commission_rate'],
            'pic' => 'https:'.$info['item']['picUrl'],
            'pics' => '',
            'desc' => '',
            'coupon_start_time' => 0,
            'coupon_end_time' => 0,
            'coupon_total_count' => 0,
            'coupon_remain_count' => 0,
            'is_tmall' => 0,
            'sales' => 0,
        ];
        if ($info['retStatus']==0){//有券
            $return['coupon_start_time'] = $info['coupon_start_time'];
            $return['coupon_end_time'] = $info['coupon_end_time'];
            $return['coupon_total_count'] = $info['coupon_total_count'];
            $return['coupon_remain_count'] = $info['coupon_remain_count'];
            $return['quan_price'] = get_coupon_price($info['coupon_info']);
        }
        $return['price_after_quan'] = round($return['price'] - $return['quan_price'],2);
        
        //官方的详情，这只主要获取轮播图
         $options = [
            'num_iids' => $item_id
         ];
         $tk = new \ddj\Tk($options);
         $item_info = $tk->info_get();
         $re = $item_info['results']['n_tbk_item'];
         if ($re){
             $return['pics'] = $re['small_images']['string'];
             $return['is_tmall'] = $re['user_type'];
             $return['sales'] = $re['volume'];
         }
         
        //商品详情接口,不能curl,本地去调用请求
        //$url = 'http://hws.m.taobao.com/cache/wdesc/5.0/?id='.$id;已经不能用
        //http://h5api.m.taobao.com/h5/mtop.taobao.detail.getdesc/6.0/?data={"id":"567900112670"}能用
        $url = 'https://h5api.m.taobao.com/h5/mtop.taobao.detail.getdesc/6.0/?data={"id":"'.$item_id.'","type":"1"}';
//         $re = http_get($url);
//         $re = json_decode($re,true);
//         //echo $re['data']['pcDescContent'];die;
//         if ($re){
//             $return['desc'] = str_replace('src="','src="https:',html($re['data']['pcDescContent']));
//         }
        $return['desc'] = $url;
        $return['desc_cb'] = "['data']['pcDescContent']";
        cache('taobao_item_info_'.$item_id,$return,3600*12);
        
        return $return;
        
    }
    /**
     * 京东-商品详情
     */
    protected function jd_item_info($item_id,$coupon_url='')
    {
        if (empty($item_id)) $this->error('id can`t be null');
        $cache_data = cache('jd_item_info_'.$item_id.$coupon_url);
        if ($cache_data){
            return $cache_data;
        }
        $return = [
            'item_id' => $item_id,
            'title' => '',
            'price' => 0,
            'quan_price' => 0,
            'price_after_quan' => 0,
            'rate' => 0,
            'pic' => '',
            'pics' => '',
            'desc' => '商品详情请点击领券前往京东查看',
            'coupon_start_time' => 0,
            'coupon_end_time' => 0,
            'coupon_total_count' => 0,
            'coupon_remain_count' => 0,
            'is_tmall' => 2,
            'sales' => 0,
        ];
        //接口查询
        $jd = new \ddj\Jd();
        $res = $jd->item_info($item_id);
        if (isset($res['getpromotioninfo_result']['result'][0])){
            $info = $res['getpromotioninfo_result']['result'][0];
            $return['title'] = $info['goodsName'];
            $return['rate'] = $info['commisionRatioWl'];
            $return['pic'] = $info['imgUrl'];
            $return['pics'] = [$info['imgUrl']];
            $return['price'] = $info['wlUnitPrice'];
            $return['sales'] = $info['inOrderCount'];
            $return['price_after_quan'] = $return['price'];
            //查券,待扩展，如果没有传券链接，调用查券接口，主要是订单里点开商品
            if ($coupon_url!=''){
                $re2 = $jd->coupon_info($coupon_url);
                if (isset($re2['getinfo_result']['data'][0])){
                    $info = $re2['getinfo_result']['data'][0];
                    $return['coupon_start_time'] = date('Y-m-d',substr($info['beginTime'], 0,-3));
                    $return['coupon_end_time'] = date('Y-m-d',substr($info['endTime'], 0,-3));
                    $return['coupon_total_count'] = $info['num'];
                    $return['coupon_remain_count'] = $info['remainNum'];
                    $return['quan_price'] = $info['discount'];
                    $return['price_after_quan'] = round($return['price'] - $return['quan_price'],2);
                }
            }
            $return['desc'] = 'https://wqsitem.jd.com/detail/'.$item_id.'_d19640715895_normal.html';
            $return['desc_cb'] = "";
            //dump($return);die;
            cache('jd_item_info_'.$item_id.$coupon_url,$return,3600*12);
            return $return;
        }
        return false;
    }
    /**
     * 拼多多-商品详情,数据非常完整
     */
    protected function pdd_item_info($item_id)
    {
        if (empty($item_id)) $this->error('id can`t be null');
        $cache_data = cache('pdd_item_info_'.$item_id);
        if ($cache_data){
            return $cache_data;
        }
        $options = [
            'type' => 'pdd.ddk.goods.detail',
            'goods_id_list' => '['.$item_id.']',
        ];
        //接口查询
        $pdd = new \ddj\Pdd($options);
        $res = $pdd->request();
        $res = $res ?? [];
        if (count($res['goods_detail_response']['goods_details'])>0){
            $info = $res['goods_detail_response']['goods_details'][0];
            $return = [
                'item_id' => $item_id,
                'title' => $info['goods_name'],
                'price' => $info['min_group_price']/100,
                'quan_price' => $info['coupon_discount']/100??0,
                'price_after_quan' => ($info['min_group_price'] - $info['coupon_discount'] ?? 0)/100,
                'rate' => $info['promotion_rate']/10,
                'pic' => $info['goods_image_url'],
                'pics' => $info['goods_gallery_urls'],
                'desc' => $info['goods_desc'],
                'coupon_start_time' => date('Y-m-d',$info['coupon_start_time']),
                'coupon_end_time' => date('Y-m-d',$info['coupon_end_time']),
                'coupon_total_count' => $info['coupon_total_quantity'],
                'coupon_remain_count' => $info['coupon_remain_quantity'],
                'is_tmall' => 3,
                'sales' => $info['sold_quantity'],
            ];
            //$return['pic'] = str_replace('http://', 'https://',$return['pic']);
            cache('pdd_item_info_'.$item_id,$return,3600*12);
            return $return;
        }else {
            return false;
        }
        
    }
    
    /**
     * 领券购买，自购 全部转换链接
     * 淘宝-要导入pid到数据库然后分配
     * 京东与拼多多调接口生成
     */
    public function buy()
    {
        $tjp = input('tjp','t');
        $item_id = input('item_id');
        $re = false;
        switch ($tjp){
            case 't':
                $re = $this->taobao_buy($item_id);
                break;
            case 'j':
                $re = $this->jd_buy($item_id,input('url'));
                break;
            case 'p':
                $re = $this->pdd_buy($item_id);
                break;
            default:
                $re = $this->taobao_buy($item_id);
        }
        if ($re)
            $this->success('获取成功','',$re);
        else
            $this->error('服务器繁忙，请稍候再试~');
    }
    /**
     * 淘宝购买
     * 任何人自购全部用系统的，统一找回订单绑定订单号，以区别分享赚/也可以借用pid后再解绑,2貌似淘宝授权后可以知道用户uid,通过转换得到订单号后六位，嗯，发现新大陆
     * 因为淘宝pid有限，所以分享赚时才绑定pid,订单同步时，订单号相同为自购，pid等自己的为分享赚，京东与拼多多不受影响
     */
    protected function taobao_buy($item_id)
    {
        if (empty($item_id)) $this->error('item_id can`t be null');
        
        $db_config = get_db_config(true);
        $mm_pid = $db_config['tk_cfg']['taobao']['pid'];
        /*
        //多多客,并且没有绑定订单号6位的，先借用一个，待订单导入后再解绑
        if ( ($this->uinfo->getData('leval')>0 && empty($this->uinfo->tb_pid)) || ($this->uinfo->getData('leval')==0 && empty($this->uinfo->tb_order6)) ){
            $pid_info = model('Pid')->get(['uid'=>0,'type'=>0]);
            if ($pid_info){
                $this->uinfo->tb_pid = $pid_info->pid;
                $this->uinfo->save();
                $pid_info->uid = $this->uid;
                $pid_info->save();
                
                $mm_pid = $pid_info->pid;
            }else {
                $this->error('推广位没有了，请联系客服');
            }
        }else {
            if ($this->uinfo->getData('leval')>0){
                $mm_pid = $this->uinfo->tb_pid;
            }else {
                //使用系统的
                $mm_pid = $db_config['tk_cfg']['taobao']['pid'];
            }
        }
        */
        
        //高佣转链
        $tk = new \ddj\Tk(['pid'=>$mm_pid]);
        $res = $tk->gaoyong($item_id);
        if (!$res){
            $this->error('服务器繁忙，请稍候再试~');
        }
        $data = [];
        $data['pid'] = $mm_pid;
        $data['adzoneid'] = str2arr($mm_pid,'_')[3];
        $data['appkey'] = $db_config['tk_cfg']['taobao']['appkey'];
        $data['return_url'] = $res['return_url'];
        
        return $data;
    }
    /**
     * 京东购买,人手一个pid,注意：京东是用推广位id(int)跟单，不是三段试pid
     */
    protected function jd_buy($item_id,$coupon_url)
    {
        if (empty($item_id) || empty($coupon_url)) $this->error('item_id or coupon_url can`t be null');
        //创建一个pid
        if (empty($this->uinfo->jd_pid)){
            $jd = new \ddj\Jd();
            $pid = $jd->create_promotion_site_batch($this->uid);
            if ($pid){
                $this->uinfo->jd_pid = $pid;
                $this->uinfo->save();
                $option = ['pid'=>$pid];
            }else {
                return false;
            }
        }else {
            $option = ['pid'=>$this->uinfo->jd_pid];
        }
        //接口查询
        $jd = new \ddj\Jd($option);
        $res = $jd->get_code_buy_uniodid($item_id, $coupon_url);
        if ($res){
            $data = [
                'return_url' => $res,
            ];
            return $data;
        }
        return false;
        
    }
    /**
     * 拼多多，人手一个pid
     */
    public function pdd_buy($item_id,$custom_parameters='')
    {
        if (empty($item_id)) $this->error('item_id can`t be null');
        /*
         * 有自定义参数支付不需要每人一个推广位
        //创建一个pid
        if (empty($this->uinfo->pdd_pid)){
            $options = [
                'type' => 'pdd.ddk.goods.pid.generate',
                'number' => 1,
                'p_id_name_list' => '[u_'.$this->uid.']'
            ];
            //接口查询
            $pdd = new \ddj\Pdd($options);
            $res = $pdd->request();
            $pid = $res['p_id_generate_response']['p_id_list'][0]['p_id'] ?? 0;
            if ($pid){
                $this->uinfo->pdd_pid = $pid;
                $this->uinfo->save();
            }else {
                return false;
            }
        }else {
            $pid = $this->uinfo['pdd_pid'];
        }
        */
        //缓存处理
        $cache_url = cache('pdd_url_'.$this->uid.'_'.$item_id);
        if ($cache_url){
            return [
                'return_url' => $cache_url,
            ];
        }
        $db_config = get_db_config(true);
        $pid = $db_config['tk_cfg']['pdd']['pid'];
        //生成推广链接
        $options = [
            'type' => 'pdd.ddk.goods.promotion.url.generate',
            'p_id' => $pid,
            'goods_id_list' => '['.$item_id.']',
            'generate_short_url' => 'true',
            'custom_parameters' => $custom_parameters ?: 'zg_'.$this->uid,//自定义参数，为链接打上自定义标签。自定义参数最长限制64个字节,貌似不用生成推广位
        ];
        //接口查询
        $pdd = new \ddj\Pdd($options);
        $res = $pdd->request();
        if (isset($res['error_response'])){
            return false;
        }
        $data = [
            //'return_url' => $res['goods_promotion_url_generate_response']['goods_promotion_url_list'][0]['short_url'],
            //唤起微信app推广短链接
            'return_url' => $res['goods_promotion_url_generate_response']['goods_promotion_url_list'][0]['we_app_web_view_short_url'],
        ];
        cache('pdd_url_'.$this->uid.'_'.$item_id,$data['return_url']);
        return $data;
    }
    /**
     * 分享
     */
    public function share()
    {
        $tjp = input('tjp','t');
        $item_id = input('item_id');
        $re = false;
        switch ($tjp){
            case 't':
                $re = $this->taobao_share($item_id);
                break;
            case 'j':
                $re = $this->jd_share($item_id,input('url'));
                break;
            case 'p':
                $re = $this->pdd_share($item_id);
                break;
            default:
                $re = $this->taobao_share($item_id);
        }
        if ($re){
            $this->success('获取成功','',$re);
        }else {
            $this->error('服务器繁忙，请稍候再试~');
        }
                
    }
    /**
     * 淘宝分享
     * 针对淘宝的分享赚，生成新的pid
     * 如何识别是分享的？购买单号与自己的单号不一致则为分享赚
     * 如果是多多客分享，则pid用上级的pid,上级是买手或达人、公司的pid没有则用系统的
     * 生成淘口令，图片二维码
     */
    protected function taobao_share($item_id)
    {
        if (empty($item_id)) $this->error('item_id can`t be null');
        $db_config = get_db_config(true);
        $mm_pid = $db_config['tk_cfg']['taobao']['pid'];
        //商品详情
        $info = self::taobao_item_info($item_id);
        if (!$info) return false;
        //如是是多多客  上级的pid
        if (empty($this->uinfo->tb_pid)){
            if ($this->uinfo->getData('leval')==0){
                $mm_pid = model('User')->get_prev_pid('tb_pid',$this->uinfo['pid']);
                if (!$mm_pid){
                    //使用系统 的,沉淀
                    $mm_pid = $db_config['tk_cfg']['taobao']['pid'];
                }
            }else {
                $pid_info = model('Pid')->get(['uid'=>0,'type'=>0]);
                if ($pid_info){
                    $this->uinfo->tb_pid = $pid_info->pid;
                    $this->uinfo->save();
                    $pid_info->uid = $this->uid;
                    $pid_info->save();
                    
                    $mm_pid = $pid_info->pid;
                }else {
                    $this->error('推广位没有了，请联系客服');
                }
            }
        }else {
            $mm_pid = $this->uinfo->tb_pid;
        }
        
        //重新生成高佣信息
        //高佣转链
        $tk = new \ddj\Tk(['pid'=>$mm_pid]);
        $res = $tk->gaoyong($item_id);
        if (!$res){
            $this->error('服务器繁忙，请稍候再试~!');
        }
        $info['return_url'] = $res['return_url'];
        //dump($res);
        //生成淘口令
        $options = [
            'text' => $info['title'],
            'url' => $info['return_url'],
            'logo' => $info['pic'],
        ];
        $tk = new \ddj\Tk($options);
        $tkl = $tk->create_tkl($options);
        if ($tkl){
            //文案
            $info['text'] = "{$info['title']}<br/>\n【在售价】{$info['price']}<br/>\n【券后价】{$info['price_after_quan']}<br/>\n----------------------------<br/>\n";
            $info['text'] .= "复制这条信息，{$tkl}，打开【手机淘宝】即可领券下单";
            //入库
            $tkl_model = model('Tkl');
            $tkl_info = $tkl_model->get(['uid'=>$this->uid,'item_id'=>$info['item_id']]);
            if (!$tkl_info){
                $tkl_info = [
                    'uid' => $this->uid,
                    'title' => $info['title'],
                    'tkl' => $tkl,
                    'con' => $info['text'],
                    'item_id' => $item_id,
                    'pic' => $info['pic'],
                    'pics' => json_encode($info['pics']),
                    'price' => $info['price'],
                    'quan_price' => $info['quan_price']
                ];
                $tkl_model->save($tkl_info);
                $tkl_info['id'] = $tkl_model->id;
            }
            //分享域名
            $doamin_url = $db_config['tk_cfg']['taobao']['share_domain'];
            //$info['share_url'] = $doamin_url.'/share_tb/'.$tkl_info['id'];
            //改为纯文字
            $info['share_url'] = str_replace('<br/>', '', $info['text']);
            $info['tkl'] = $tkl;
            //dump($tkl_info);
            //self::build_tk_hb($info);
            //生成推广海报
            return $info;
        }else {
            return false;
        }
        
    }
    /**
     * 京东分享
     * 如何识别分享赚？再重新生成一个pid?
     * 多多客分享用上级的pid,节省pid创建，最终订单变成上级的分享订单
     */
    protected function jd_share($item_id,$coupon_url='')
    {
        if (empty($item_id) || empty($coupon_url)) $this->error('item_id or coupon_url can`t be null');
        $db_config = get_db_config(true);
        $mm_pid = $db_config['tk_cfg']['jd']['pid'];
        //商品详情
        $info = self::jd_item_info($item_id,$coupon_url);
        if (!$info) return false;
        //如是是多多客  上级的pid
        if (empty($this->uinfo->jd_share_pid)){
            if ($this->uinfo->getData('leval')==0){
                $mm_pid = model('User')->get_prev_pid('jd_share_pid',$this->uinfo['pid']);
                if (!$mm_pid){
                    //使用系统 的,沉淀
                    $mm_pid = $db_config['tk_cfg']['jd']['pid'];
                }
            }else {
                //创建
                $jd = new \ddj\Jd(['space_name'=>'s_'.$this->uid]);
                $mm_pid = $jd->create_promotion_site_batch($this->uid);
                if ($mm_pid){
                    $this->uinfo->jd_share_pid = $mm_pid;
                    $this->uinfo->save();
                }else {
                    $this->error('推广位创建失败，请联系客服');
                }
            }
        }else {
            $mm_pid = $this->uinfo->jd_share_pid;
        }
        //获取高佣链接
        //接口查询
        $jd = new \ddj\Jd(['pid'=>$mm_pid]);
        $res = $jd->get_code_buy_uniodid($item_id, $coupon_url);
        if ($res){
            //文案
            $info['text'] = "{$info['title']}<br/>\n【在售价】{$info['price']}<br/>\n【券后价】{$info['price_after_quan']}<br/>\n----------------------------<br/>\n";
            $info['text'] .= $res."<br/>\n点击链接打开页面后，即可领券购买";
            $info['share_url'] = $res;
            return $info;
        }
        //dump($mm_pid);
        //dump($info);die;
    }
    /**
     * 拼多多分享
     * 如何识别分享赚？链接可以带参数
     */
    protected function pdd_share($item_id)
    {
        if (empty($item_id)) $this->error('item_id can`t be null');
        $db_config = get_db_config(true);
        $mm_pid = $db_config['tk_cfg']['pdd']['pid'];
        //商品详情
        $info = self::pdd_item_info($item_id);
        if (!$info) return false;
        //生成推广链接
        $options = [
            'type' => 'pdd.ddk.goods.promotion.url.generate',
            'p_id' => $mm_pid,
            'goods_id_list' => '['.$item_id.']',
            'generate_short_url' => 'true',
            'custom_parameters' => 'share_'.$this->uid,//自定义参数，为链接打上自定义标签。自定义参数最长限制64个字节,貌似不用生成推广位
        ];
        //接口查询
        $pdd = new \ddj\Pdd($options);
        $res = $pdd->request();
        if (isset($res['error_response'])){
            return false;
        }
        //$info['share_url'] = $res['goods_promotion_url_generate_response']['goods_promotion_url_list'][0]['short_url'];
        $info['share_url'] = $res['goods_promotion_url_generate_response']['goods_promotion_url_list'][0]['we_app_web_view_short_url'];
        //文案
        $info['text'] = "{$info['title']}<br/>\n【在售价】{$info['price']}<br/>\n【券后价】{$info['price_after_quan']}<br/>\n----------------------------<br/>\n";
        $info['text'] .= $info['share_url']."<br/>\n点击链接打开页面后，即可领券购买";
        return $info;
    }
    
    /**
     * 生成海报
     * 算了，不写了，发现app端可以生成,体验好
     */
    private function build_tk_hb($info)
    {
        //header ( 'Content-type: image/png' );
        $save_path = './uploads/share_item_hb/'.$this->uid;
        //创建画布1080*1920
        $image = imagecreatetruecolor(1080, 1920);
        //背景颜色
        $bg_color = imagecolorallocate($image, 251, 251, 251);
        imagefill($image, 0, 0, $bg_color);
        //加上标题及图标
        $icons = ['taobao.png','tmall.png','jd.png','pinduoduo.png'];
        $icon_img = imagecreatefrompng('./static/images/'.$icons[$info['is_tmall']]);
        imagecopyresampled($image, $icon_img, 80, 100, 0, 0, 40, 40, 200, 200);
        //标题;
        $fontfile = './static/font/msyh.ttf';
        $fontfile = realpath($fontfile);
        $title_color = imagecolorallocate($image, 0, 0, 0);
        //imagettftext($image, $size, $angle, $x, $y, $color, $fontfile, $text);
        if (mb_strlen($info['title'])>27){
            imagefttext($image, 24, 0, 125, 130, $title_color, $fontfile, mb_substr($info['title'], 0,27));
            imagefttext($image, 24, 0, 125, 180, $title_color, $fontfile, mb_substr($info['title'], 27));
        }else {
            imagefttext($image, 24, 0, 125, 130, $title_color, $fontfile, $info['title']);
        }
        //加上背景图
        $pic_img = imagecreatefromjpeg($info['pic']);
        $pic_size = getimagesize($info['pic']);
        //dump($size);die;
        imagecopyresampled($image, $pic_img, 80, 300, 0, 0, 920, 920, $pic_size[0], $pic_size[1]);
        //加上二维码
        //$cqrimg = imagecreatefrompng($save_path.'_qrcode.png');
        //imagecopyresampled($cbgimg, $cqrimg, 340, 1220, 0, 0, 400, 400, 400, 400);
        //保存
        //imagepng($image);
        imagepng($image,$save_path.'_hb.png');
        imagedestroy($image);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
