<?php
// +----------------------------------------------------------------------
// | duoduojie app system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 duoduojie app All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\api\controller;
use think\Controller;
use app\common\controller\Sms;
use ddj\Random;
use think\facade\Cache;
use app\common\model\Orders;
use app\common\model\OrdersMoney;
use app\common\model\User as UserMode;
/**
 * 用户相关控制器 需要登录认证
 * @author zqs
 *
 */
class User extends Controller
{
    public $uid;
    public $uinfo;
    /**
     * 无需登录的方法,同时也就不需要鉴权了
     * @var array
     */
    protected $noNeedLogin = ['user/smslogin','user/sendsms','user/wxlogin'];
    /**
     * 初始化验证token
     */
    public function initialize()
    {
        parent::initialize();
        $controllername = strtolower($this->request->controller());
        $actionname = strtolower($this->request->action());
        
        $path = $controllername . '/' . $actionname;
        if (!in_array($path, $this->noNeedLogin)){
             $token = input('token');
             $uid = input('uid');
             if (empty($token) || empty($uid)){
                 $this->result('请登录！',501);
             }
             $uinfo = model('User')->get(['uid'=>$uid,'token'=>$token]);
             if (!$uinfo){
                 $this->result('请登录',502);
             }
             $this->uinfo = $uinfo;
             $this->uid = $uid;
             //检测会员过期
             if ($this->uinfo->getData('leval')>0 && $this->uinfo->leval_end_time > 0 && $this->uinfo->leval_end_time < time() ){
                 $this->uinfo->leval = 0;
                 $this->uinfo->leval_end_time = 0;
                 $this->uinfo->save();
             }
        }
    }
    /**
     * 短信验证码登录
     */
    public function smslogin()
    {
        $mobile = input('post.mobile');
        $smscode = input('post.smscode');
        $device_id = input('post.device_id','');
        $device_model = input('post.device_model','');
        $invite_code = input('post.invite_code');
        if (empty($mobile)) $this->error('请输入手机号');
        if (empty($smscode)) $this->error('请输入短信验证码');
        
        //判断验证码是否正确
        $smsfind = model('Sms')->where('used',0)->where('type','reglogin')->where('mobile',$mobile)->order('create_time desc')->find();
        //添加苹果审核测试账号
        if ($mobile=='13673649458'){
            $smsfind = [
                'code' => '{"code":"12345"}'
            ];
        }
        if ($smsfind){

            $codearr = json_decode($smsfind['code'],true);
            if ($codearr['code']==$smscode){

                //时效性验证  15分内有效
                if ($mobile!='13673649458'){
                    if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
                        $this->error('验证码已过期，请重新获取');
                    }
                }

                //开始注册或者登录
                $User = model('User');
                $uinfo = $User->where('mobile',$mobile)->find();
                if ($uinfo){//登录
                    if ($uinfo['status']!=1){
                        $this->error('您的账户涉嫌违规操作，已被禁用');
                    }
                    unset($uinfo['password']);
                    //更新用户信息，返回用户信息
                    $uinfo->last_time = time();
                    $uinfo->last_ip = request()->ip();
                    $uinfo->last_device_id = $device_id;
                    $uinfo->last_device_model = $device_model;
                    $uinfo->login_count +=1;
                    //换token,之前登录失效
                    $token = md5(Random::uuid().time().Random::alnum(6));
                    $uinfo->token = $token;
                    $uinfo->save();
                    $re = [
                        'token' => $uinfo['token'],
                        'uid' => $uinfo['uid'],
                        'is_bind_mobile' => 1,
                    ];
                    if ($mobile!='13673649458'){
                        //使验证码失效
                        $smsfind->used = 1;
                        $smsfind->save();
                    }


                    $this->success('登录成功','',$re);
                }else {
                    //创建用户  
                    $data = [
                        'nickname' => $mobile,
                        'mobile' => $mobile,
                        'token' => md5(Random::uuid().time().Random::alnum(6)),
                        'reg_ip' => request()->ip(),
                        'last_device_id' => $device_id,
                        'last_device_model' => $device_model,
                        'login_count' => 1,
                        'invite_code' => make_invite_code()
                    ];
                    
                    //如果有填写有邀请吗
                    if (!empty($invite_code)){
                        $pid = db('User')->where(['status'=>1,'invite_code'=>$invite_code])->value('uid');
                        if ($pid){
                            $data['pid'] = $pid;
                        }else {
                            //检测邀请记录
                            $invite_logs = model('InviteLogs')->get(['mobile'=>$mobile]);
                            if ($invite_logs){
                                $data['pid'] = $invite_logs->uid;
                                $invite_logs->is_bind = 1;
                                $invite_logs->save();
                            }else {
                                $this->error('推荐码不正确');
                            }
                            
                        }
                    }else {
                        //检测邀请记录
                        $invite_logs = model('InviteLogs')->get(['mobile'=>$mobile]);
                        if ($invite_logs){
                            $data['pid'] = $invite_logs->uid;
                            $invite_logs->is_bind = 1;
                            $invite_logs->save();
                        }else {
                            //是否强制邀请码注册
                            $need_code = intval(get_db_config()['invite_need_code']);
                            if ($need_code==1){
                                $this->error('请填写邀请码');
                            }
                        }
                        
                    }
                    
                    if ($User->save($data)){
                        $re = [
                            'token' => $data['token'],
                            'uid' => $User->uid,
                            'is_bind_mobile' => 1,
                        ];

                        //邀请送积分
                        if ($data['pid']>0){
                        	score_invite($data['pid']);
                            // 发通知
                            pushOffline($data['pid'],$data);

                        }

                        //监听任务邀请粉丝
                        add_task_score(10, $data['pid']);
                        add_task_score(5, $data['pid']);
                        //使验证码失效
                        $smsfind->used = 1;
                        $smsfind->save();
                        // 同步用户uid
                        syncUid($User->uid);
                        $this->success('注册成功','',$re);
                    }else {
                        $this->error('注册失败,请稍后再试~');
                    }
                }
            }else {
                $this->error('验证码错误');
            }
        }else {
            $this->error('验证码错误~');
        }
    }
    /**
     * 微信快捷登录
     */
    public function wxlogin()
    {
        $unionid = input('post.unionid');
        $wxopen_openid = input('post.openid');
        if (empty($unionid)||empty($wxopen_openid)) $this->result('',0,'参数错误');
        $device_id = input('post.device_id','');
        $device_model = input('post.device_model','');
        //开始注册或者登录
        $User = model('User');
        //$uinfo = $User->where('unionid',$unionid)->where('wxopen_openid',$wxopen_openid)->find();
        $uinfo = $User->where('unionid',$unionid)->find();
        if ($uinfo){//登录
            if ($uinfo['status']!=1){
                $this->result('',0,'您的账户涉嫌违规操作，已被禁用');
            }
            /*
            //是否有上级
            if (empty($uinfo['pid'])){
                $invite_code = (int)input('post.invite_code');
                if (empty($invite_code)){
                    $this->result(input('post.'),2,'请输入推荐码.');
                }else {
                    $pid = db('User')->where(['status'=>1,'invite_code'=>$invite_code])->value('uid');
                    if ($pid && $pid!=$uinfo['uid']){
                        $uinfo->pid = $pid;
                    }else {
                        $this->result(input('post.'),2,'推荐码不正确.');
                    }
                }
            }
            */
            unset($uinfo['password']);
            //更新用户信息，返回用户信息
            $uinfo->last_time = time();
            $uinfo->last_ip = request()->ip();
            $uinfo->last_device_id = $device_id;
            $uinfo->last_device_model = $device_model;
            $uinfo->login_count +=1;
            //换token,之前登录失效
            $token = md5(Random::uuid().time().Random::alnum(6));
            $uinfo->token = $token;
            $uinfo->save();
            $re = [
                'token' => $uinfo['token'],
                'uid' => $uinfo['uid'],
                'pid' => $uinfo['pid'],
                'is_bind_mobile' => empty($uinfo['mobile']) ? 0 :1, 
            ];
            $this->result($re,1,'登录成功');
        }else {
            //创建用户
            $wx_info = input('post.');
            $data = [
                'mobile' => '',
                'token' => md5(Random::uuid().time().Random::alnum(6)),
                'reg_ip' => request()->ip(),
                'last_device_id' => $device_id,
                'last_device_model' => $device_model,
                'login_count' => 1,
                'wxopen_openid' => $wxopen_openid,
                'unionid' => $unionid,
                'nickname' => $wx_info['nickname'],
                'sex' => $wx_info['sex'],
                'province' => $wx_info['province'],
                'city' => $wx_info['city'],
                'headimgurl' => $wx_info['headimgurl'],
                'invite_code' => make_invite_code()
            ];
            //验证推荐码注册
            if (empty($wx_info['invite_code'])){
                //是否强制邀请码注册
                $need_code = intval(get_db_config()['invite_need_code']);
                if ($need_code==1){
                    $this->result($wx_info,2,'请输入推荐码');
                }
            }else {
                $pid = db('User')->where(['status'=>1,'invite_code'=>$wx_info['invite_code']])->value('uid');
                if ($pid){
                    $data['pid'] = $pid;
                }else {
                    $this->result($wx_info,2,'推荐码不正确');
                }  
            }
            if ($User->allowField(true)->save($data)){
                $re = [
                    'token' => $data['token'],
                    'uid' => $User->uid,
                    'is_bind_mobile' => 0,
                    //'pid' => $User->pid
                ];
                //邀请送积分
                if ($data['pid']>0){
                	score_invite($data['pid']);
                    pushOffline($data['pid'],$data);

                }
                //监听任务邀请粉丝
                add_task_score(10, $data['pid']);
                add_task_score(5, $data['pid']);
                // 同步用户uid
                syncUid($User->uid);

                $this->result($re,1,'注册成功');
            }else {
                $this->result('',0,'注册失败,请稍后再试~');
                
            }
        }
    }
    
    /**
     * 绑定微信 
     */
    public function bind_wechat(){
        $unionid = input('post.unionid');
        $wxopen_openid = input('post.openid');
        if (empty($unionid)||empty($wxopen_openid)) $this->error('参数错误');
        //检查是否已经被绑定
        $uinfo = db('User')->where('unionid',$unionid)->find();
        if ($uinfo){
            //提示
            $this->error('此微信已被绑定');
        }else {
            $wx_info = input('post.');
            $data = [
                'wxopen_openid' => $wxopen_openid,
                'unionid' => $unionid,
                'nickname' => $wx_info['nickname'],
                'sex' => $wx_info['sex'],
                'province' => $wx_info['province'],
                'city' => $wx_info['city'],
                'headimgurl' => $wx_info['headimgurl']
            ];
            if (db('User')->where('uid',$this->uinfo['uid'])->update($data)){
                $this->success('绑定成功');
            }else {
                $this->error('绑定失败，请稍后重试');
            }
        }
    }
    /**
     * 发送短信验证码
     * type:0注册登录 1：绑定手机
     */
    public function sendsms()
    {
        $mobile = input('post.mobile');
        if(!preg_match(config('site.regex_mobile'),$mobile)) {
            $this->error('手机号格式不正确');
        }
        //是否被拉黑
        if (db('User')->where(['mobile'=>$mobile,'status'=>0])->find()){
            $this->error('此手机已被列入黑名单');
        }
        $type = input('post.type',0);
        $sms = new Sms();
        if ($type==0){//注册登录
        	//if (!db('User')->where('mobile',$mobile)->find()){$this->error('请先使用微信登录');}
            $re = $sms->reglogin($mobile,['code'=>mt_rand(10000,99999)]);
        }elseif ($type==1){//绑定手机
            if (model('User')->get(['mobile'=>$mobile])){
                $this->error('此手机已被占用');
            }
            $re = $sms->bindmobile($mobile,['code'=>mt_rand(10000,99999)]);
        }
        
        //trace($re);
        if ($re['Code']=='OK'){
            $this->success('发送成功');
        }else {
            $this->error('短信发送失败，请稍候重试');
        }
    }
    /**
     * 获取基本信息
     */
    public function userinfo()
    {
        $db_config = get_db_config(true);
        $data = [
            'nickname' => $this->uinfo->nickname,
            'leval_text' => $this->uinfo['leval'],
            'leval' => $this->uinfo->getData('leval'),
            'money' => $this->uinfo['money'],
            'score' => $this->uinfo['score'],
            'uid' => $this->uinfo['uid'],
            'headimgurl' => empty($this->uinfo['headimgurl'])?'':$this->uinfo['headimgurl'],
            //'pid' => $this->uinfo['pid'],
        	'invite_code' => $this->uinfo['invite_code'],
            //'is_bind_tb' => empty($this->uinfo['tb_order6']) ? 1 : 0,
            'app_kf_url' => $db_config['app_kf_url'],
            'app_xsjc_url' => $db_config['app_xsjc_url'],
            'app_help_url' => $db_config['app_help_url'],
            'app_center_ad' => $db_config['app_center_ad'],
            'is_bind_wx' => empty($this->uinfo['wxopen_openid']) ? 0 : 1,
            'is_bind_mobile' => empty($this->uinfo['mobile']) ? 0 :substr_replace($this->uinfo->mobile,'****',3,4),
        ];
        //本月预估
        $month_yg = db('orders_money')->where('uid',$this->uid)->where('status',0)->whereTime('create_time', 'month')->sum('money');
        //本月结算
        $month_js = db('orders_money')->where('uid',$this->uid)->where('status',1)->whereTime('create_time', 'month')->sum('money');
        $data['month_yg'] = $month_yg;
        $data['month_js'] = $month_js;
        $this->result($data,1);
    }
    
    /**
     * 我的足迹/收藏
     */
    public function history()
    {
        $p = input('p',1);
        $type = input('type',0);
        $list = model('GoodsLogs')->where('uid',$this->uinfo['uid'])->where('is_collect',$type)->order('update_time desc')->page($p,10)->select();
        $list = $list ?? [];
        if (count($list)>0){
            $this->result($list,1);
        }else {
            $this->error('没有更多了~');
        }
    }
    /**
     * 删除收藏或者足迹
     */
    public function history_del()
    {
        $ids = input('ids','');
        if (empty($ids)){
            $this->error('请选择你要操作的数据');
        }
        $del = db('goods_logs')->where('id','in',$ids)->where('uid',$this->uid)->delete();
        $this->success('操作成功');
    }
    /**
     * 记录并绑定订单后6位
     */
    public function pay_succ_cb()
    {
        die();
    	//[127735983069974659,127735983069974650]
    	trace(input('post.'));
    	$status = input('post.status');
    	if ($status=='true' || $status=='1'){
    		$orderids = input('post.ordersId');
    		$orderids = rtrim(ltrim($orderids,'['),']');
    		$orderids = str_replace(['"','\''], '', $orderids);
    		$orderid_arr = str2arr($orderids);
    		$orderid_arr = array_filter(array_unique($orderid_arr));
            $orderid_arr = $orderid_arr ?? [];
    		if (count($orderid_arr)>0){
    			$list = [];
    			$tb_order6 = 0;
    			foreach ($orderid_arr as $v){
    				$li = [
    						'uid' => $this->uinfo['uid'],
    						'orderid' => $v,
    				];
    				$list[] = $li;
    				$tb_order6 = substr($v, -6);
    			}
    			//绑定订单后6位
    			if (empty($this->uinfo['tb_order6'])){
    			    $this->uinfo->tb_order6 = $tb_order6;
    			    $this->uinfo->save();
    			}
    			//批量写入
    			model('OrderLocal')->saveAll($list);
    		}
    	}
    	$this->success('');//妈的，上线需要响应
    }
    /**
     * 生成海报
     */
    public function hb(){

        $db_config = get_db_config();
        //生成海报

        $re = self::build_hb($this->uid);
        $is_down = 1;

        //if ($re==false) $is_down = 0;

        // 暂时从数据库获取海报url
        $db_config = get_db_config(true);
        $domain_share = $db_config['domain_share'];

        $data = [
            'title' => $db_config['invite_share_title'],
            'content' => $db_config['invite_share_content'],

            'url' => $domain_share.'/invite/'.$this->uinfo['invite_code'],
            'haibao' => config('site.domain').'/uploads/qr/'.$this->uid.'_hb.png',
            'is_down' => $is_down,
        ];
        $this->result($data,1);
    }
    /**
     * 生成海报
     */
    private function build_hb($uid)
    {
        $cache = cache('hb_'.$uid);

        if ($cache)
            return false;
        cache('hb_'.$uid,1,'','haibao');
        //生成当前的二维码
        $save_path = './uploads/qr/'.$uid;
        //if (!file_exists($save_path.'_qrcode.png')){
        if(1){

            // 暂时从数据库获取海报url
            $db_config = get_db_config(true);
            $domain_share = $db_config['domain_share'];

            $url = $domain_share.'/invite/'.$this->uinfo['invite_code'];
            $qrCode = new \Endroid\QrCode\QrCode($url);
            $qrCode->setSize(380);

            // Save it to a file
            $qrCode->writeFile($save_path.'_qrcode.png');
        }
        //生成海报
        $db_config = get_db_config();
        $bgimg = get_img($db_config['app_share_hb'],'url');
        //创建画布1080*1920
        $cbgimg = imagecreatetruecolor(1080, 1920);
        //加上背景图
        $cbg_img = imagecreatefrompng('.'.$bgimg);
        imagecopyresampled($cbgimg, $cbg_img, 0, 0, 0, 0, 1080, 1920, 1080, 1920);
        //加上二维码
        $cqrimg = imagecreatefrompng($save_path.'_qrcode.png');
        imagecopyresampled($cbgimg, $cqrimg, 340, 1220, 0, 0, 400, 400, 400, 400);
        //保存
        imagepng($cbgimg,$save_path.'_hb.png');
        imagedestroy($cbgimg);
        return true;
    }
    /**
     * 我的钱包
     */
    public function wallet(){
        $money_count = db('MoneyLogs')->where('uid',$this->uinfo['uid'])->where('money','gt',0)->sum('money');//累积收益 ,可能有退款的误差，不过无所谓了
        $shoudan_count = db('ShoudanLogs')->where('pid',$this->uinfo['uid'])->where('status',1)->sum('money');//累积首单收益
        $pay_count = db('Orders')->where('uid',$this->uinfo['uid'])->whereIn('order_status', '1,3')->sum('pay_fee');//累积消费
        $data = [
            'money_count' => $money_count,
            'shoudan_count' => $shoudan_count,
            'pay_count' => $pay_count,
            'money' => $this->uinfo['money'],
            'sd_money' => $this->uinfo['sd_money'],
            'mobile' => empty($this->uinfo['mobile'])?0:1,
            'wxopen_openid' => empty($this->uinfo['wxopen_openid'])?0:1,
            'leval' => $this->uinfo->getData('leval'),
        ];
        $this->result($data,1);
    }
    /**
     * 资金明细
     * 1：余额明细
     * 2：提现记录
     */
    public function money_logs()
    {
        $type = input('type',1);
        $p = input('p',1);
        switch ($type){
            case 2:
                //提现记录
                $list = db('Draw')->where('uid',$this->uid)->where('status','neq',-1)->order('id desc')->page($p,10)->select();
                if ($list){
                    foreach ($list as $k=>$v){
                        $list[$k]['title'] = '提现到'.$v['draw_type']===0?'微信':'支付宝';
                        $status_text = ['<font>审核中</font>','<font color="#21ba45">提取成功</font>','<font color="#f5334c">驳回</font>','<font color="#21ba45">提取成功</font>'];
                        $list[$k]['status_text'] = $status_text[$v['status']];
                        $list[$k]['time'] = date('m-d H:i:s',$v['create_time']);
                    }
                }
                break;
            case 1:
                //收益明细，所有记录
                $list = db('money_logs')->where('uid',$this->uinfo['uid'])->order('id desc')->page($p,15)->select();
                if ($list){
                    foreach ($list as $k=>$v){
                        $type_text = config('site.money_type')[$v['type']];
                        $list[$k]['title'] = $type_text;
                        $list[$k]['time'] = date('m-d H:i:s',$v['create_time']);
                    }
                }
                break;
            case 3:
                //积分明细
                $list = db('score_logs')->where('uid',$this->uinfo['uid'])->order('id desc')->page($p,15)->select();
                if ($list){
                    foreach ($list as $k=>$v){
                        $type_text = config('site.score_type')[$v['type']];
                        $list[$k]['title'] = $type_text;
                        $list[$k]['time'] = date('m-d H:i:s',$v['create_time']);
                    }
                }
                break;
            default:
                $this->result('');
        }
        $list = $list ?? [];
        if (count($list)>0)
            $this->result($list,1);
        else
            $this->result('');
    }
    
    /**
     * 提现配置及余额
     */
    public function draw_cfg()
    {
        $db_config = get_db_config();
        //余额提取中的
        $money_ing = db('Draw')->where('uid',$this->uinfo['uid'])->where('status',0)->sum('money_rel');
        $money_has = db('Draw')->where('uid',$this->uinfo['uid'])->where('status','in','1,3')->sum('money_rel');
        $data = [
            'money' => $this->uinfo['money'],
            'money_ing' => $money_ing,
            'money_has' => $money_has,
            'intro' => $db_config['draw_intro'],
            'alipay_name' => $this->uinfo['alipay_name'],
            'alipay_account' => $this->uinfo['alipay_account'] ?: '',
            'is_bind_wx' => empty($this->uinfo['wxopen_openid']) ? 0 : $this->uinfo->nickname,
        ];
        $this->result($data,1);
    }
    /**
     * 提现
     */
    public function draw()
    {
        if (cache('draw_uid_'.$this->uid)){
            $this->error('请不要频繁操作');
        }
        cache('draw_uid_'.$this->uid, 'lock', 3);//3秒只能转一次
        $db_config = get_db_config(true);
        $money = floatval(input('post.money'));
        $type = intval(input('post.type'));
        if ($type!==0 && $type!==1){
            $this->error('请选择提现方式');
        }
        if ($money<=0){
            $this->error('提现金额不正确');
        }
        //账户是否正常
        if ($this->uinfo['status']!=1){
            $this->error('账户异常~请联系客服');
        }
        if ($type===1){
            //是否强制支付宝提现
            if (empty($this->uinfo['alipay_name'])||empty($this->uinfo['alipay_account'])){
                $this->error('请先绑定支付宝');
            }
        }elseif ($type===0){
            $this->error('暂不支持微信提现，请选择支付宝！');
            if (empty($this->uinfo->wxopen_openid)){
                $this->error('请先绑定微信');
            }
        }
        
        //最小提现金额限制 整数倍
        if ($money<$db_config['draw_money_min']){
            $this->error('提现金额不能小于最小提现金额');
        }
        if (($money*100)%($db_config['draw_money_min_int']*100) != 0){
            $this->error('提现金额须是'.$db_config['draw_money_min_int'].'的整数倍!');
        }
        //时间限制
        if (date('G')<$db_config['draw_money_time_start'] || date('G')>=$db_config['draw_money_time_end']){
            $this->error('非提现时间:'.$db_config['draw_money_time_start'].'-'.$db_config['draw_money_time_end']);
        }
        if ($this->uinfo['money']<$money){
            $this->error('账户余额不足');
        }
        $money_type = 'draw_money';
        $money_text = '余额提现';
        $res = add_money($money_type, $this->uinfo['uid'],['money'=>-$money,'remark'=>$money_text]);
        if ($res){
            $data = [
                'uid' => $this->uinfo['uid'],
                'mobile' => $this->uinfo['mobile'],
                'real_name' => $this->uinfo['alipay_name'],
                'alipay_account' => $this->uinfo['alipay_account'],
                'draw_type' => $type,
            ];
            $data['money_rel'] = $money;
            $data['money'] = $money*(100-intval($db_config['app_draw_per']))/100;//手续费后的，即实际到账的
            $re = model('Draw')->save($data);
            if ($re){
                $this->success('提现成功');
            }else {
                $this->success('提现失败，请稍候再试');
            }
            
        }else {
            $this->success('提现失败，请稍候再试！');
        }
    }
    /**
     * 我的团队
     */
    public function team()
    {
        $leval_arr = get_db_config(true)['user_leval_cfg']['name']??config('site.user_leval');
        $user_model = new UserMode();
        //一级总粉丝
        $fans1 = $user_model->where('pid',$this->uid)->column('uid');
        //一级珍粉外的总粉丝
        $fans1_1 = [];
        if ($fans1){
            $fans1_1 = $user_model->where('pid',$this->uid)->where('leval','>',0)->column('uid');
        }
        //二级总粉丝
        $fans2 = [];
        //二级珍粉外的总粉丝
        $fans2_2 = [];
        if ($fans1){
            $fans2 = $user_model->whereIn('pid',$fans1)->column('uid');
            $fans2_1 = $user_model->whereIn('pid',$fans1)->where('leval','>',0)->column('uid');
        }
        $pinfo = model('User')->get($this->uinfo['pid']);
        
        $data = [];
        $data['total'] = count($fans1)+count($fans2);
        //$data['total_dr'] = $leval_arr[2].$user_model->where('pid',$this->uid)->where('leval',2)->count();
        //$data['total_ms'] = $leval_arr[1].$user_model->where('pid',$this->uid)->where('leval',1)->count();
        $data['today_add'] = $user_model->where('pid',$this->uid)->whereTime('create_time','today')->count();
        $data['yesterday_add'] = $user_model->where('pid',$this->uid)->whereTime('create_time','yesterday')->count();
        //$data['one_count'] = $data['total'];
        $data['pname'] = $pinfo['nickname'] ?? '系统';
        $data['pmobile'] = $pinfo['mobile'] ?? '';
        $data['pheadimgurl'] = $pinfo['headimgurl'] ?? '';
        $data['fans1_1'] = count($fans1_1);
        $data['fans2_1'] = count($fans2_1);
        $data['fans1_2'] = count($fans1)-count($fans1_1);
        $data['lv0_name'] = $leval_arr[0] ?? '珍粉';
        $this->result($data,1);
    }
    /**
     * 粉丝列表
     */
    public function team_list()
    {
        $p = input('p',1);
        $m = input('m','');
        if (empty($m)){
            $list = model('User')->where('pid',$this->uid)->order('uid desc')->page($p,10)->select();
        }else {
            $list = model('User')->where('pid',$this->uid)->where('mobile',$m)->order('uid desc')->page($p,10)->select();
        }
        $list = $list ?? [];
        if (count($list)>0){
            foreach ($list as $k=>$v){
                if (empty($v['headimgurl'])){
                    $list[$k]['headimgurl'] = '';
                }
                //推荐人数
                $list[$k]['fans'] = model('User')->where('pid',$v['uid'])->count();
                //订单数量
                $list[$k]['orders'] = model('Orders')->where('uid',$v['uid'])->count();
                //今日预估
                $list[$k]['today_yg'] = model('OrdersMoney')->where('uid',$v['uid'])->where('status',0)->whereTime('create_time', 'today')->sum('money');
                //今日新增
                $list[$k]['today_add'] = model('OrdersMoney')->where('uid',$v['uid'])->where('status',1)->whereTime('create_time', 'today')->sum('money');
                //本月预估
                $list[$k]['month_yg'] = model('OrdersMoney')->where('buy_uid',$v['uid'])->where('status',0)->whereTime('create_time', 'month')->sum('money');
            }
            $this->success('获取成功','',$list);
        }else {
            $this->error('没有了~');
        }
    }
    /**
     * 我的订单
     */
    public function order()
    {
        //监听任务 订单相关，在这里触发了先
        add_task_score(2, $this->uid);
        $tjp = input('tjp','t');
        $p = input('p',1);
        $type = input('type',1);
        $map = [];
        $map[] = ['tjp','=',$tjp];
        if ($type>1 && $type<5){
            $relation_status = [2=>1,3=>2];
            if ($type==4){
                $map[] = ['order_status','in','3,5'];
            }else {
                $map[] = ['order_status','=',$relation_status[$type]];
            }
        }
        //粉丝订单
        $is_fans = input('is_fans',0);
        if ($is_fans==1){
            //我的一级粉丝
            $son_uid = db('user')->where('pid',$this->uid)->column('uid');
            if ($son_uid){
                $map[] = ['uid','in',$son_uid];
            }else {
                $this->error('没有了');
            }
        }else {
            $map[] = ['uid','=',$this->uid];
        }
        //trace($map);
        $list = model('Orders')->where($map)->order('id desc')->field('id,tjp,item_id,title,pic,price,pay_price,create_time,trade_id,order_status,order_type')->page($p,10)->select();
        $list->append(['order_status_text']);
        $list = $list ?? [];
        if (count($list)>0){
            $this->result($list,1);
        }else {
            $this->error('没有了');
        }
    }
    
    /**
     * 填写推荐码后登录,即绑定上级
     */
    public function bind_pid()
    {
        $invite_code = input('invite_code');
        if (!empty($invite_code)){
            $pid = db('User')->where(['status'=>1,'invite_code'=>$invite_code])->value('uid');
            if ($pid){
                if ($this->uinfo->pid){
                    $this->error('您已经有啦~');
                }else {
                    $this->uinfo->pid = $pid;
                    $this->uinfo->save();
                    $this->success('激活成功');
                }
            }else {
                $this->error('推荐码不正确');
            }  
        }else {
            $this->error('请填写推荐码');
        }
    }
    
    
    /**
     * 绑定手机
     */
    public function bind_mobile()
    {
        $mobile = input('post.mobile');
        $smscode = input('post.smscode');
        if (empty($mobile)) $this->error('请输入手机号');
        if (empty($smscode)) $this->error('请输入短信验证码');
        
        //判断验证码是否正确
        $smsfind = model('Sms')->where('used',0)->where('type','bindmobile')->where('mobile',$mobile)->order('create_time desc')->find();
        if ($smsfind){
            $codearr = json_decode($smsfind['code'],true);
            if ($codearr['code']==$smscode){
                //时效性验证  15分内有效
                if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
                    $this->error('验证码已过期，请重新获取');
                }
                //检测邀请记录
                $invite_logs = model('InviteLogs')->get(['mobile'=>$mobile]);
                if ($invite_logs && $this->uinfo->pid==0){
                    $this->uinfo->pid = $invite_logs->uid;
                    $invite_logs->is_bind = 1;
                    $invite_logs->save();
                    pushOffline($invite_logs->uid,['pid'=>$invite_logs->uid,'nickname'=>$this->uinfo->nickname]);
                }
                //开始绑定
                $this->uinfo->mobile = $mobile;
                $this->uinfo->save();
                //使验证码失效
                $smsfind->used = 1;
                $smsfind->save();
                
                $this->success('绑定成功');
            }else {
                $this->error('验证码错误');
            }
        }else {
            $this->error('验证码错误~');
        }
    }
    /**
     * 绑定支付宝
     */
    public function bind_alipay()
    {
        $type = input('type',0);
        if (empty($this->uinfo['mobile'])){
            $this->result('',2,'请先绑定手机号');
        }
        if ($type==1){
            //发送短信
            $sms = new Sms();
            $re = $sms->bindalipay($this->uinfo['mobile'], ['code'=>mt_rand(10000,99999)]);
            if ($re['Code']=='OK'){
                $this->success('发送成功');
            }else {
                $this->error('短信发送失败，请稍候重试');
            }
        }else {
            //提交
            $real_name = input('post.real_name');
            $alipay_account = input('post.alipay_account');
            $smscode = input('post.smscode');
            if (empty($real_name)) $this->error('请输入真实姓名');
            if (empty($alipay_account)) $this->error('请输入支付宝账号');
            if (empty($smscode)) $this->error('请输入短信验证码');
            //判断验证码是否正确
            $smsfind = model('Sms')->where('used',0)->where('type','bindalipay')->where('mobile',$this->uinfo['mobile'])->order('create_time desc')->find();
            if ($smsfind){
                $codearr = json_decode($smsfind['code'],true);
                if ($codearr['code']==$smscode){
                    //时效性验证  15分内有效
                    if ( (time()-$smsfind->getData('create_time')) > (15*60) ){
                        $this->error('验证码已过期，请重新获取');
                    }
                    //开始绑定
                    $this->uinfo->alipay_name = $real_name;
                    $this->uinfo->alipay_account = $alipay_account;
                    $this->uinfo->save();
                    //使验证码失效
                    $smsfind->used = 1;
                    $smsfind->save();
                    $this->success('绑定成功');
                }else {
                    $this->error('验证码错误');
                }
            }else {
                $this->error('验证码错误~');
            }
        }
    }
    
    /**
     * 收入报表
     */
    public function order_count()
    {
        $order_model = new Orders();
        $money_model = new OrdersMoney();
        //TODO这里可以扩展时间
        $map = [];
        $map[] = ['uid','=',$this->uid];
        
        $data = [];
        //总收益
        $data['total'] = $money_model->where($map)->sum('money');
        //我的订单收益 
        $data['total_my'] = $money_model->where($map)->where('buy_uid','=',$this->uid)->sum('money');
        //团队总收益
        $data['total_td'] = $money_model->where($map)->where('buy_uid','<>',$this->uid)->sum('money');
        
        //我的订单收益，淘京拼
        $data['my_tb'] = $money_model->where($map)->where('buy_uid','=',$this->uid)->where('tjp','t')->sum('money');
        $data['my_tb_num'] = $order_model->where($map)->where('order_status','in','1,2,4')->where('tjp','t')->count();
        $data['my_jd'] = $money_model->where($map)->where('buy_uid','=',$this->uid)->where('tjp','j')->sum('money');
        $data['my_jd_num'] = $order_model->where($map)->where('order_status','in','1,2,4')->where('tjp','j')->count();
        $data['my_pdd'] = $money_model->where($map)->where('buy_uid','=',$this->uid)->where('tjp','p')->sum('money');
        $data['my_pdd_num'] = $order_model->where($map)->where('order_status','in','1,2,4')->where('tjp','p')->count();
        //我的团队订单收益 
        $data['td_tb'] = $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','t')->sum('money');
        $data['td_tb_num'] =  $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','t')->count();
        $data['td_jd'] = $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','j')->sum('money');
        $data['td_jd_num'] =  $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','j')->count();
        $data['td_pdd'] = $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','p')->sum('money');
        $data['td_pdd_num'] =  $money_model->where($map)->where('buy_uid','<>',$this->uid)->where('tjp','p')->count();
        
        $this->success('获取成功','',$data);
    }
    /**
     * 绑定支付宝信息
     */
    public function bind_taobao()
    {
        $tb_openid = input('openId');
        if ($tb_openid){
            //没有授权过
            if (empty($this->uinfo->tb_openid)){
                $find = db('user')->where('tb_openid',$tb_openid)->find();
                if (!$find){
                    //获取订单后6位
                    $tb_order_id = input('userid');
                    //头像取法，淘宝已经加密了.pass
                    /*
                    $avatarUrl = input('avatarUrl');
                    $urlarr=parse_url($avatarUrl);
                    parse_str($urlarr['query'],$query);
                    $userId = $query['userId'];
                    */
                    //topAccessToken取法
                    $tb_order_id6 = substr(input('topAccessToken'), -6);
                    $userId = $tb_order_id ?? $tb_order_id6;
                    if ($userId){
                        $tb_order6 = substr($userId, -6,2).substr($userId, -2,2).substr($userId, -4,2);
                        $this->uinfo->tb_order6 = $tb_order6;
                    }
                    
                    $this->uinfo->tb_openid = $tb_openid;
                    //如果没有绑定微信
                    if (empty($this->uinfo->wxopen_openid)){
                        $this->uinfo->nickname = input('nick');
                        $this->uinfo->headimgurl = input('avatarUrl');
                    }
                    $this->uinfo->save();
                }else {
                    $this->error('此淘宝账号已授权其它账号，请勿重复授权');
                }
            }else {
                //授权过，检测是否是当前淘宝账号
                if ($this->uinfo->tb_openid!=$tb_openid){
                    $this->error('当前账号已经绑定其它淘宝账号，请用原淘宝账号授权');
                }
            }
            
        }else {
            $this->error('授权失败');
        }
        /*
        $avatarUrl = input('avatarUrl');
        $urlarr=parse_url($avatarUrl);
        parse_str($urlarr['query'],$query);
        $userId = $query['userId'];
        $update = false;
        if (isset($userId)){
            $tb_order6 = substr($userId, -6,2).substr($userId, -2,2).substr($userId, -4,2);
            if (empty($this->uinfo->tb_order6)){
                if (!db('user')->where('tb_order6',$tb_order6)->find()){
                    $this->uinfo->tb_order6 = $tb_order6;
                    $update = true;
                }
            }
        }
        //如果没有绑定微信
        if (empty($this->uinfo->wxopen_openid)){
            $this->uinfo->nickname = input('nick');
            $this->uinfo->headimgurl = input('avatarUrl');
            $update = true;
        }
        if ($update){
            $this->uinfo->save();
        }
        */
        
        $this->success('');
    }
    /**
     * 输出购买等级信息
     */
    public function pay_info()
    {
        $type = (int)input('type',0);
        if ($type==0){
            $lvcfg = get_db_config(true)['user_leval_cfg'];
            $li  = '';
            $pay1 = floatval($lvcfg['leval_condition'][1][1]??0);//299礼包
            $pay2 = floatval($lvcfg['leval_condition'][1][2]??0);//会员月付
            $pay3 = floatval($lvcfg['leval_condition'][2][2]??0);//vip月付
            $pay4 = floatval($lvcfg['leval_condition'][3][2]??0);//分公司月付
            if ($pay1>0){
                $li .= '<li tapmode checkCode="1" class="active" data-payitem="1"><span>'.($lvcfg['name'][1]??'级别1').'</span><b>'.$pay1.'</b>元/永久  <img src="../image/payBtn1.png"></li>';
            }
            if ($pay2>0){
                $li .= '<li tapmode checkCode="0" data-payitem="2"><span>'.($lvcfg['name'][1]??'级别1').'</span><b>'.$pay2.'</b>元/月  <img src="../image/payBtn0.png"></li>';
            }
            if ($pay3>0){
                $li .= '<li tapmode checkCode="0" data-payitem="3"><span>'.($lvcfg['name'][2]??'级别2').'</span><b>'.$pay3.'</b>元/年  <img src="../image/payBtn0.png"></li>';
            }
            if ($pay4>0){
                $li .= '<li tapmode checkCode="0" data-payitem="4"><span>'.($lvcfg['name'][3]??'级别3').'</span><b>'.$pay4.'</b>元/年  <img src="../image/payBtn0.png"></li>';
            }
            $this->success('获取成功','',$li);
        }else {
            $db_config = get_db_config(true);
            $lvcfg = $db_config['user_leval_cfg'];
            $sq_cfg = $db_config['app_center_ad'];
            $a = new Article();
            $content1 = replace_con_img($a->index($sq_cfg['intro_top'])['content']);
            $content2 = replace_con_img($a->index($sq_cfg['intro_bottom'])['content']);
            $data = [
                'content1' => $content1,
                'content2' => $content2,
                'leval_name' => $lvcfg['name'],
                'leval_condition' => $lvcfg['leval_condition'],
                'lv1_count' => db('user')->where('pid',$this->uid)->where('leval',1)->count(),
                'lv2_count' => db('user')->where('pid',$this->uid)->where('leval',2)->count(),
            ];
            
            return $this->result($data,1);
        }
        
    }
    /**
     * 支付宝 h5支付 
     */
    public function alipay_h5()
    {
        $payitem = (int)input('payitem');
        if ($this->uinfo->getData('leval')>=3){
            $this->error('您的等级过高，无需升级');
        }
        if (!in_array($payitem, [1,2,3,4])){
            $this->error('请选择要购买的项目');
        }
        $pay_lv = $payitem;
        if ($payitem>1){
            $pay_lv = $pay_lv-1;
        }
        if ($this->uinfo->getData('leval')>=$pay_lv){
            $this->error('您的等级大于所购买等级，无需再次购买');
        }
        //begin
        $db_config = get_db_config(true);
        $lvcfg = $db_config['user_leval_cfg'];
        $pay1 = floatval($lvcfg['leval_condition'][1][1]??0);//299礼包 永久
        $pay2 = floatval($lvcfg['leval_condition'][1][2]??0);//会员月付
        $pay3 = floatval($lvcfg['leval_condition'][2][2]??0);//vip月付
        $pay4 = floatval($lvcfg['leval_condition'][3][2]??0);//分公司月付
        $item_str = 'pay'.$payitem;
        $pay_money = $$item_str;
        if ($pay_money<=0){
            $this->error('此选项已暂停支付');
        }
        //记录日志
        $out_trade_no = build_order_no().$this->uid;//26+5=31位
        
        $data = [
            'uid' => $this->uinfo['uid'],
            'pay_type' => 2,
            'out_trade_no' => $out_trade_no,
            'amount' => $pay_money,
            'leval' => $pay_lv,
            'pay_item' => $payitem,
        ];
        //支付配置
        $alipay_cfg_wap = $db_config['alipay_cfg_wap'];
        if (model('Paylogs')->save($data)){
            //支付宝h5支付
            include env('extend_path').'/payment/alipay/AopSdk.php';
            $aop = new \AopClient ();
            $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
            $aop->appId = $alipay_cfg_wap['app_id'];
            $aop->rsaPrivateKey = $alipay_cfg_wap['merchant_private_key'];
            $aop->alipayrsaPublicKey=$alipay_cfg_wap['alipay_public_key'];
            $aop->apiVersion = '1.0';
            $aop->postCharset='UTF-8';
            $aop->format='json';
            $aop->signType='RSA2';
            $request = new \AlipayTradeWapPayRequest();
            $content = [
                'body' => '日用百货',
                'subject' => '日用百货',
                'out_trade_no' => $out_trade_no,
                'timeout_express' => '30m',
                'total_amount' => $pay_money,
                'product_code' => 'QUICK_WAP_WAY',
            ];
            $bizcontent = json_encode($content);
            $notify_url = config('site.domain').'/api/notify/alipay_h5';
            $request->setNotifyUrl($notify_url);
            $request->setReturnUrl($alipay_cfg_wap['return_url']);
            $request->setBizContent($bizcontent);
            $result = $aop->pageExecute ( $request);
            //echo $result;
            $this->success($result);
        }else {
            $this->error('服务器繁忙，请稍候再试~');
        }
    }
    /**
     * 代理中心
     */
    public function agent()
    {
        $db_config = get_db_config(true);
        //我的一级粉丝
        $son_uid = db('user')->where('pid',$this->uid)->column('uid');
        $ids = $son_uid;
        $ids[]= $this->uid;
        $data = [
            'nickname' => $this->uinfo->nickname,
            'headimgurl' => $this->uinfo->headimgurl??'',
            'leval_text' => $this->uinfo->leval,
            'leval_end_time' => $this->uinfo->leval_end_time==0?'永久有效':date('Y-m-d',$this->uinfo->leval_end_time),
            'total_money' => db('money_logs')->where('uid',$this->uid)->where('money','>',0)->sum('money'),//累计收益
            'total_order' => db('orders')->where('uid','in',$ids)->count(),
            'total_fans' => count($son_uid),
            'app_kf_url' => $db_config['app_kf_url'],
            'rate_zigou' => $db_config['user_leval_cfg']['zigou'][$this->uinfo->getData('leval')]??0,
            'rate_fenyong' => $db_config['user_leval_cfg']['libao'][$this->uinfo->getData('leval')][1]??0,
        ];
        $this->success('获取成功','',$data);
    }
    /**
     * 智齿客服
     */
    public function zhichi()
    {
        $db_config = get_db_config(true);
        $param = [
            'appkey' => $db_config['app_zhichi']['appkey'],
            'userId' => $this->uid,
            'nickName' => $this->uinfo->nickname,
            'phone' => $this->uinfo->mobile,
            //'customInfo' => $this->uinfo,
            'isShowEvaluate' => true,//点击返回时是否弹出满意度评价
            'isSettingSkillSet' => false,//不知道干啥的
            'skillSetId' => '',//技能组编号，根据传递的值转接到对应的技能组，可选
            'isShowTansfer' => true,//机器人优先模式，是否直接显示转人工按钮(值为NO时，会在机器人无法回答时显示转人工按钮)
            'titleFont' => '18.0',//顶部标题颜色、评价标题，可为空 (仅适用iOS)
            'backgroundColor' => "#f0f0f0",//对话页面背景，可为空 (仅适用iOS)
            'topViewTextColor' => "#FFFFFF",//顶部文字颜色(返回、标题)，可为空 (仅适用iOS)
            'isCustomLinkClick' => false,//自己处理消息中的链接，如果设置为1，将通过callBack返回到页面ret=1,value为link实际地址，desc为描述
            'themeColor' => '#f0392b',//可设置头部颜色; 提交评价问题选中背景色以及提交评价按钮背景色; 聊天内容中，相似问题字体颜色和富文本类型中“阅读全文”字体颜色，可为空 (仅适用Android)
            'avatarUrl' => $this->uinfo->headimgurl?:'',//用户头像，可为空
        ];
        $this->success('获取成功','',['param'=>$param]);
    }
    /**
     * 兑吧相关
     */
    public function duiba()
    {
        $db_config = get_db_config(true);
        $duiba = new \ddj\Duiba();
        $autologin_url = $duiba->buildCreditAutoLoginRequest(
            $db_config['app_duiba']['appkey'], 
            $db_config['app_duiba']['appsecret'], 
            $this->uid, $this->uinfo->score
            );
        $sign_url = $duiba->buildRedirectAutoLoginRequest(
            $db_config['app_duiba']['appkey'], 
            $db_config['app_duiba']['appsecret'], 
            $this->uid, $this->uinfo->score, 
            $db_config['app_duiba']['sign_url'] ?? ''
            );
        $data = [
            'autologin_url' => $autologin_url,
            'sign_url' => $sign_url,
        ];
        $custom_url = input('custom_url');
        if (!empty($custom_url)){
        	$data['custom_url'] = $duiba->buildRedirectAutoLoginRequest(
	        			$db_config['app_duiba']['appkey'],
	        			$db_config['app_duiba']['appsecret'],
	        			$this->uid, $this->uinfo->score,
	        			$custom_url
        			);
        }
        $this->success('获取成功','',$data);
    }
    /**
     * 任务
     */
    public function task()
    {
        if ($this->request->isPost()){
            $type = input('type');
            model('ShareLogs')->save(['uid'=>$this->uid,'type'=>intval($type)]);
            //触发商品和app分享
            add_task_score(3, $this->uid);
            add_task_score(4, $this->uid);
            add_task_score(8, $this->uid);
            add_task_score(9, $this->uid);
            $this->success('获取成功');
        }
    	$db_config = get_db_config(true);
    	$task_cfg = $db_config['user_leval_cfg']['score']['task'];
    	//每日任务
    	//7.浏览n件商品
    	$days_7 = db('goods_logs')->where(['uid'=>$this->uid,'is_collect'=>0])->whereTime('create_time', 'today')->count();
    	$task_cfg['days'][7]['finish'] = $days_7;
    	//8.完成n次app分享
    	$days_8 = model('ShareLogs')->where(['uid'=>$this->uid,'type'=>0])->whereTime('create_time', 'today')->count();
    	$task_cfg['days'][8]['finish'] = $days_8;
    	//9.完成n次商品分享
    	$days_9 = model('ShareLogs')->where(['uid'=>$this->uid,'type'=>1])->whereTime('create_time', 'today')->count();
    	$task_cfg['days'][9]['finish'] = $days_9;
    	//10.邀请1位有效粉丝
    	$days_10 = model('User')->where('pid',$this->uid)->whereTime('create_time', 'today')->count();
    	$task_cfg['days'][10]['finish'] = $days_10;
    	//11.邀请好友购买会员
    	$task_cfg['days'][11]['condition'] = 1;
    	$myfans = model('User')->where('pid',$this->uid)->column('uid');
    	if ($myfans){
    	    $days_11 = model('Paylogs')->where('uid','in',$myfans)->where('pay_status',1)->whereTime('create_time', 'today')->count();
    	    $task_cfg['days'][11]['finish'] = $days_11;
    	}else {
    	    $task_cfg['days'][11]['finish'] = 0;
    	}
    	//新手任务
    	//1.购买n个有效订单
    	$newuser_1 = model('Orders')->where('uid',$this->uid)->where('is_share',0)->where('order_status','not in','3,5')->count();
    	$task_cfg['newuser'][1]['finish'] = $newuser_1;
    	//2.分享购买n个有效订单
    	$newuser_2 = model('Orders')->where('uid',$this->uid)->where('is_share',1)->where('order_status','not in','3,5')->count();
    	$task_cfg['newuser'][2]['finish'] = $newuser_2;
    	//3.完成n次app分享
    	$newuser_3 = model('ShareLogs')->where(['uid'=>$this->uid,'type'=>0])->count();
    	$task_cfg['newuser'][3]['finish'] = $newuser_3;
    	//4.完成n次商品分享
    	$newuser_4 = model('ShareLogs')->where(['uid'=>$this->uid,'type'=>1])->count();
    	$task_cfg['newuser'][4]['finish'] = $newuser_4;
    	//5.邀请n位有效粉丝
    	$newuser_5 = model('User')->where('pid',$this->uid)->count();
    	$task_cfg['newuser'][5]['finish'] = $newuser_5;
    	//6.首次购买会员
    	$task_cfg['newuser'][6]['finish'] = 0;
    	$task_cfg['newuser'][6]['condition'] = 1;
    	$newuser_6 = model('Paylogs')->where('uid',$this->uid)->where('pay_status',1)->count();
    	if ($newuser_6>0){
    	    $task_cfg['newuser'][6]['finish'] = 1;
    	}
    	
    	//dump($task_cfg);die;
    	$data = [
    		'score' => $this->uinfo['score'],
    		'rules' => $task_cfg,
    	];
    	$this->success('获取成功','',$data);
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
