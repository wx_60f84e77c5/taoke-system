<?php
// +----------------------------------------------------------------------
// | llpt system
// +----------------------------------------------------------------------
// | Copyright (c) 2017 llpt All rights reserved.
// +----------------------------------------------------------------------
// | Author: Gooe <zqscjj@163.com> QQ:81009953
// +----------------------------------------------------------------------
namespace app\job;
use think\queue\Job;
use app\common\model\Orders;
use app\common\model\User;
use app\common\model\OrdersMoney;
class Dojob
{
	/**
	 * 处理订单
	 * 淘京拼分开了，后续想扩展可以直接写
	 */
    public function handle_order(Job $job,$data)
    {
        //trace($data);
        //删除此条任务
        $job->delete();
        $queue_type = $data['queue_type']??'import';
        if ($queue_type=='fanli'){
            $this->fanli($data);
        }else {
            $this->import_order($data);
        }
    }
    /**
     * 返利计算
     */
    public function fanli($data)
    {
        $order_id = $data['order_id'];
        $order_money = new OrdersMoney();
        $list = $order_money->all(['order_id'=>$order_id,'status'=>0]);
        $list = $list ?? [];
        if (count($list)>0){
            $tjp = ['t'=>'淘宝','j'=>'京东','p'=>'拼多多'];
            foreach ($list as $k=>$v){
                if (add_money('fanli', $v->uid,['money'=>$v->money,'remark'=>$tjp[$v->tjp].'订单结算','attach'=>$order_id])){
                    $v->status = 1;
                    $v->save();
                }
            }
        }
        
    }
	/**
	 * 导入订单
	 */
	public function import_order($data)
	{
	    $order_model = new Orders();
	    $get = $order_model->get(['trade_id'=>$data['trade_id'],'tjp'=>$data['tjp']]);
	    if ($get){//更新订单
	        //不再更新主图
	        unset($data['pic']);
	        //注：这里京东与拼多我没有找回订单的功能，因为如果在app里使用，这种情况是不存在的
	        if ($data['tjp']=='t'){
	            //如果是淘宝订单，如果是自购的，以前没有绑定，现在却绑定的情况 下
	            if ($get->uid==0){
	                //是否为自购订单
	                $order6 = substr($data['trade_id'], -6);
	                $uid = User::where('tb_order6',$order6)->value('uid');
	                if ($uid){
	                    //自购订单,找回
	                    $data['uid'] = $uid;
	                    //分佣补上
	                    if (in_array($data['order_status'], [1,2,4])){
	                        fenyong_order($get->id, $uid, $data['income_price'],$data['tjp']);
	                    }
	                }
	            }
	            //更新订单信息
	            $get->save($data);
	        }elseif ($data['tjp']=='j'){//如果是京东订单
	            //更新订单信息
	            $get->save($data);
	        }elseif ($data['tjp']=='p'){//如果是拼多多订单
	            //更新订单信息
	            $get->save($data);
	        }
	    }else {//导入订单
	        //如果是淘宝订单
	        if ($data['tjp']=='t'){
	            //获取商品主图
	            //官方的详情，这只主要获取轮播图
	            $options = [
	                'num_iids' => $data['item_id'],
	            ];
	            $tk = new \ddj\Tk($options);
	            $item_info = $tk->info_get();
	            $re = $item_info['results']['n_tbk_item'];
	            if ($re){
	                $data['pic'] = $re['pict_url'];
	            }
	            trace($re);
	            //是否为自购订单
	            $order6 = substr($data['trade_id'], -6);
	            $uid = User::where('tb_order6',$order6)->value('uid');
	            if ($uid){
	                //自购订单
	                $data['uid'] = $uid;
	                $order_model->save($data);
	                //分佣计算，并且订单是付款、结算、成功的情况下才分佣
	                if (in_array($data['order_status'], [1,2,4])){
	                    fenyong_order($order_model->id, $uid, $data['income_price'],$data['tjp']);
	                }
	            }else {
	                //判断是否为分享订单，凡是通过pid找到的为分享订单
	                $share_uid = User::where('tb_pid',$data['p_id'])->value('uid');
	                if ($share_uid){
	                	$data['uid'] = $share_uid;
	                    $data['is_share'] = 1;
	                    $order_model->save($data);
	                    //分佣计算分享
	                    if (in_array($data['order_status'], [1,2,4])){
	                    	fenyong_order($order_model->id, $share_uid, $data['income_price'],$data['tjp'],1);
	                    }
	                }else {//无主定单，等待找回
	                    $order_model->save($data);
	                }
	            }
	        }elseif ($data['tjp']=='j'){//如果是京东订单
	            //商品主图
	            //接口查询
	            $jd = new \ddj\Jd();
	            $res = $jd->item_info($data['item_id']);
	            $data['pic'] = $res['getpromotioninfo_result']['result'][0]['imgUrl'] ?? '';
	            //trace($res);
	            //是否为自购订单
	            $uid = User::where('jd_pid',$data['p_id'])->value('uid');
	            if ($uid){
	                //自购订单
	                $data['uid'] = $uid;
	                $order_model->save($data);
	                //分佣计算，并且订单是付款、结算、成功的情况下才分佣
	                if (in_array($data['order_status'], [1,2,4])){
	                    fenyong_order($order_model->id, $uid, $data['income_price'],$data['tjp']);
	                }
	            }else {
	                //判断是否为分享订单，凡是通过pid找到的为分享订单
	                $share_uid = User::where('jd_share_pid',$data['p_id'])->value('uid');
	                if ($share_uid){
	                    $data['uid'] = $share_uid;
	                    $data['is_share'] = 1;
	                    $order_model->save($data);
	                    //分佣计算分享
	                    if (in_array($data['order_status'], [1,2,4])){
	                    	fenyong_order($order_model->id, $share_uid, $data['income_price'],$data['tjp'],1);
	                    }
	                }else {//无主定单，等待找回,理论上是不存在的
	                    $order_model->save($data);
	                }
	            }
	        }elseif ($data['tjp']=='p'){//如果是拼多多订单
	            //是否为自购订单还是分享
	            if (!empty($data['p_id'])){
	                list($type,$uid) = str2arr($data['p_id'],'_');
	                $uinfo = User::where('uid',$uid)->find();
	                if ($uinfo){
	                    $data['uid'] = $uid;
	                    if ($type=='zg'){
	                        $order_model->save($data);
	                        fenyong_order($order_model->id, $uid, $data['income_price'],$data['tjp']);
	                    }elseif ($type=='share'){
	                        $data['is_share'] = 1;
	                        $order_model->save($data);
	                        fenyong_order($order_model->id, $uid, $data['income_price'],$data['tjp'],1);
	                    }
	                }else {//无主定单
	                    $order_model->save($data);
	                }
	            }else {//非app内产生的订单
	                $order_model->save($data);
	            }
	        }
	    }
	}
	
	
	
	
	
	
	
	
	
	
	
}