<?php /*a:1:{s:73:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/index.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title><?php echo htmlentities($site['name']); ?></title>
    <link rel="stylesheet" href="/static/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/static/font-awesome/css/font-awesome.min.css" media="all" />
    <link rel="stylesheet" href="/static/css/app.css" media="all" />
    <link rel="stylesheet" href="/static/css/themes/green.css" media="all" id="skin" kit-skin/>
    <link rel="stylesheet" href="/static/css/common.css" media="all" />
</head>

<body class="kit-theme">
    <div class="layui-layout layui-layout-admin kit-layout-admin">
        <div class="layui-header">
            <div class="layui-logo"><?php echo htmlentities($site['name']); ?></div>
            <div class="layui-logo kit-logo-mobile">13673649458</div>
            <ul class="layui-nav layui-layout-left kit-nav"></ul>
            <ul class="layui-nav layui-layout-right kit-nav">
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <i class="layui-icon">&#xe63f;</i> 皮肤
                    </a>
                    <dl class="layui-nav-child skin">
                        <dd><a href="javascript:;" data-skin="default" style="color:#393D49;"><i class="layui-icon">&#xe658;</i> 默认</a></dd>
                        <dd><a href="javascript:;" data-skin="orange" style="color:#ff6700;"><i class="layui-icon">&#xe658;</i> 橘子橙</a></dd>
                        <dd><a href="javascript:;" data-skin="green" style="color:#00a65a;"><i class="layui-icon">&#xe658;</i> 原谅绿</a></dd>
                        <dd><a href="javascript:;" data-skin="pink" style="color:#FA6086;"><i class="layui-icon">&#xe658;</i> 少女粉</a></dd>
                        <dd><a href="javascript:;" data-skin="blue.1" style="color:#00c0ef;"><i class="layui-icon">&#xe658;</i> 天空蓝</a></dd>
                        <dd><a href="javascript:;" data-skin="red" style="color:#dd4b39;"><i class="layui-icon">&#xe658;</i> 枫叶红</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;" id="clearCached">清除缓存</a>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;">
                        <?php if($admin['headimg'] == '0'): ?>
                        <img src="/static/images/0.jpg" class="layui-nav-img" />
                        <?php else: ?>
                        <img src="<?php echo htmlentities(get_img($admin['headimg'])); ?>" class="layui-nav-img" />
                        <?php endif; ?>
                        <?php echo htmlentities($admin['nickname']); ?>
                    </a>
                    <dl class="layui-nav-child">
                        <dd><a href="javascript:;" kit-target data-options="{ url:'<?php echo url('User/index'); ?>',icon:'&#xe658;',title:'基本资料',id:'999'}"><span>基本资料</span></a></dd>
                        <dd><a href="javascript:;" kit-target data-options="{ url:'<?php echo url('User/index'); ?>',icon:'&#xe658;',title:'安全设置',id:'999'}"><span>安全设置</span></a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item"><a href="<?php echo url('logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a></li>
            </ul>
        </div>

        <div class="layui-side layui-bg-black kit-side">
            <div class="layui-side-scroll">
                <div class="kit-side-fold"><i class="fa fa-navicon" aria-hidden="true"></i></div>
                <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
                <ul class="layui-nav layui-nav-tree" lay-filter="kitNavbar" kit-navbar>
                	<?php if(is_array($menulist) || $menulist instanceof \think\Collection || $menulist instanceof \think\Paginator): $i = 0; $__LIST__ = $menulist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
                	<li class="layui-nav-item">
                        <?php if(!(empty($vo['childlist']) || (($vo['childlist'] instanceof \think\Collection || $vo['childlist'] instanceof \think\Paginator ) && $vo['childlist']->isEmpty()))): ?>
                        <a class="" href="javascript:;" ><i class="<?php echo htmlentities($vo['icon']); ?>" aria-hidden="true"></i><span> <?php echo htmlentities($vo['title']); ?></span></a>
                        <dl class="layui-nav-child">
                        	<?php if(is_array($vo['childlist']) || $vo['childlist'] instanceof \think\Collection || $vo['childlist'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['childlist'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$cld): $mod = ($i % 2 );++$i;?>
                            <dd>
                                <a href="javascript:;" data-url="<?php echo url($cld['name']); ?>" data-icon="<?php echo htmlentities($cld['icon']); ?>" data-title="<?php echo htmlentities($cld['title']); ?>" kit-target data-id='<?php echo htmlentities($cld['id']); ?>'>
                                    <i class="<?php echo htmlentities($cld['icon']); ?>" aria-hidden="true"></i><span> <?php echo htmlentities($cld['title']); ?></span></a>
                            </dd>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </dl>
                        <?php else: ?>
                        <a class="" href="javascript:;" data-url="<?php echo url($vo['name']); ?>" kit-target  data-id='<?php echo htmlentities($vo['id']); ?>' data-icon="<?php echo htmlentities($vo['icon']); ?>"  data-title="<?php echo htmlentities($vo['title']); ?>" ><i class="<?php echo htmlentities($vo['icon']); ?>" aria-hidden="true"></i><span> <?php echo htmlentities($vo['title']); ?></span></a>
                        <?php endif; ?>
                    </li>
                    <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
        <div class="layui-body" id="container">
            <!-- 内容主体区域 -->
            <div style="padding: 15px;"><i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop">&#xe63e;</i> 请稍等...</div>
        </div>

        <div class="layui-footer">
            <!-- 底部固定区域 -->
            2017 &copy;
            <a href="javascript:;"><?php echo htmlentities($site['name']); ?></a> MIT license
        </div>
    </div>

    <script src="/static/layui/layui.js"></script>
    <script>
        var message;
        layui.config({
            base: '/static/js/',
            version: '1.0.1'
        }).use(['app', 'message'], function() {
            var app = layui.app,
                $ = layui.jquery,
                layer = layui.layer;
            //将message设置为全局以便子页面调用
            message = layui.message;
            //主入口
            app.set({
                type: 'iframe'
            }).init();
            $('dl.skin > dd').on('click', function() {
                var $that = $(this);
                var skin = $that.children('a').data('skin');
                switchSkin(skin);
            });
            var setSkin = function(value) {
                    layui.data('kit_skin', {
                        key: 'skin',
                        value: value
                    });
                },
                getSkinName = function() {
                    return layui.data('kit_skin').skin;
                },
                switchSkin = function(value) {
                    var _target = $('link[kit-skin]')[0];
                    _target.href = _target.href.substring(0, _target.href.lastIndexOf('/') + 1) + value + _target.href.substring(_target.href.lastIndexOf('.'));
                    setSkin(value);
                },
                initSkin = function() {
                    var skin = getSkinName();
                    switchSkin(skin === undefined ? 'green' : skin);
                }();
            //清除缓存
            $('#clearCached').on('click', function () {
                $.post('/admin/ajax/wipecache',function(re){
                    if (re.code) {
                        layer.msg('清除完成~',{time:500},function(){
                            //location.reload();//刷新
                        })
                    }
                });
            });
        });
    </script>
</body>

</html>