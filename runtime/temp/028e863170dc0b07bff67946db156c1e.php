<?php /*a:2:{s:69:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/set/app.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
<style type="text/css">
/*.layui-form-label{width: 150px;}*/
.upcb img{width: 40px;height: 40px;}
</style>

</head>

<body>
	<div class="admin-body">
		

<div class="layui-tab layui-tab-brief" lay-filter="test">
  <ul class="layui-tab-title">
    <li class="layui-this"  lay-id="11">首页设置</li>
    <li  lay-id="22">规则与说明</li>
    <li  lay-id="33">苹果审核设置</li>
    <li  lay-id="44">登录设置</li>
    <li  lay-id="55">智齿客服</li>
    <li  lay-id="66">兑吧配置</li>
  </ul>
  <div class="layui-tab-content">
    <div class="layui-tab-item layui-show">
    	<form class="layui-form form1" action="<?php echo url('set/edit'); ?>">
    	<fieldset class="layui-elem-field layui-field-title">
		  <legend>首页轮播图</legend>
		  <div class="layui-field-box">
		  		<?php $__FOR_START_1566829885__=0;$__FOR_END_1566829885__=5;for($i=$__FOR_START_1566829885__;$i < $__FOR_END_1566829885__;$i+=1){ ?>
				<div class="layui-form-item">
					<label class="layui-form-label">图<?php echo htmlentities($i); ?>：</label>
					<div class="layui-input-inline" style="width:300px;">
					  <input type="text" name="config[app_index_banner][<?php echo htmlentities($i); ?>][url]" value="<?php echo htmlentities($app_index_banner[$i]['url']); ?>"  placeholder="请输入url/商品:id/功能名称" autocomplete="off" class="layui-input">
					</div>
					<div class="layui-input-inline">
				      <select name="config[app_index_banner][<?php echo htmlentities($i); ?>][type]">
				        <option value="">类型</option>
				        <?php $_result=config('site.app_func_type');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kk % 2 );++$kk;?>
				        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
				        <?php endforeach; endif; else: echo "" ;endif; ?>
				      </select>
				    </div>
				    <div class="layui-input-inline">
				      <select name="config[app_index_banner][<?php echo htmlentities($i); ?>][action]">
				        <option value="">app功能</option>
				        <?php $_result=config('site.app_func_action');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kkk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kkk % 2 );++$kkk;?>
				        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo['title']); ?></option>
				        <?php endforeach; endif; else: echo "" ;endif; ?>
				      </select>
				    </div>
					<div>
						<span class="upcb">
							<span class="img"><?php if(!(empty($app_index_banner[$i]['img']) || (($app_index_banner[$i]['img'] instanceof \think\Collection || $app_index_banner[$i]['img'] instanceof \think\Paginator ) && $app_index_banner[$i]['img']->isEmpty()))): ?><img src="<?php echo htmlentities(get_img($app_index_banner[$i]['img'])); ?>"><?php endif; ?></span>
							<span class="ipt"><input type="hidden" name="config[app_index_banner][<?php echo htmlentities($i); ?>][img]" value="<?php echo htmlentities($app_index_banner[$i]['img']); ?>"></span>
						</span>
						<button type="button" class="layui-btn up1">
						  <i class="layui-icon">&#xe67c;</i>上传图片
						</button>
					</div>
				</div>
				<?php } ?>
		  </div>
		</fieldset>
		<fieldset class="layui-elem-field layui-field-title">
		  <legend>图标</legend>
		  <div class="layui-field-box">
		    <?php $__FOR_START_1685781579__=0;$__FOR_END_1685781579__=10;for($i=$__FOR_START_1685781579__;$i < $__FOR_END_1685781579__;$i+=1){ ?>
		    <div class="layui-form-item">
				<label class="layui-form-label">图<?php echo htmlentities($i+1); ?>：</label>
				<div class="layui-input-inline" style="width: 100px;">
				  <input type="text" name="config[app_index_icon][<?php echo htmlentities($i); ?>][name]" value="<?php echo htmlentities($app_index_icon[$i]['name']); ?>" required   placeholder="名称" autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline" style="width: 200px;">
				  <input type="text" name="config[app_index_icon][<?php echo htmlentities($i); ?>][url]" value="<?php echo htmlentities($app_index_icon[$i]['url']); ?>" required   placeholder="请输入url/商品:id/功能名称" autocomplete="off" class="layui-input">
				</div>
				<div class="layui-input-inline">
			      <select name="config[app_index_icon][<?php echo htmlentities($i); ?>][type]">
			        <option value="">类型</option>
			        <?php $_result=config('site.app_func_type');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kk % 2 );++$kk;?>
				    <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
				    <?php endforeach; endif; else: echo "" ;endif; ?>
			      </select>
			    </div>
			    <div class="layui-input-inline">
			      <select name="config[app_index_icon][<?php echo htmlentities($i); ?>][action]">
			        <option value="">app功能</option>
			        <?php $_result=config('site.app_func_action');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kkk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kkk % 2 );++$kkk;?>
			        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo['title']); ?></option>
			        <?php endforeach; endif; else: echo "" ;endif; ?>
			      </select>
			    </div>
				<div>
					<span class="upcb">
						<span class="img"><?php if(!(empty($app_index_icon[$i]['img']) || (($app_index_icon[$i]['img'] instanceof \think\Collection || $app_index_icon[$i]['img'] instanceof \think\Paginator ) && $app_index_icon[$i]['img']->isEmpty()))): ?><img src="<?php echo htmlentities(get_img($app_index_icon[$i]['img'])); ?>"><?php endif; ?></span>
						<span class="ipt"><input type="hidden" name="config[app_index_icon][<?php echo htmlentities($i); ?>][img]" value="<?php echo htmlentities($app_index_icon[$i]['img']); ?>"></span>
					</span>
					<button type="button" class="layui-btn up1">
					  <i class="layui-icon">&#xe67c;</i>上传图片
					</button>
				</div>
			</div>
		    <?php } ?>
		  </div>
		</fieldset>
		<fieldset class="layui-elem-field layui-field-title">
		  <legend>版块</legend>
		  <div class="layui-field-box">
		  	 <div class="layui-row layui-col-space10">
			    <div class="layui-col-lg3" style="text-align: center;">
			      <img src="/static/images/bk_demo.png" width="300">
			    </div>
			    <div class="layui-col-lg9">
			    	<?php $__FOR_START_555724886__=0;$__FOR_END_555724886__=8;for($i=$__FOR_START_555724886__;$i < $__FOR_END_555724886__;$i+=1){ ?>
				    <div class="layui-form-item">
						<label class="layui-form-label">版块<?php echo htmlentities($i+1); ?>：</label>
						<div class="layui-input-inline" style="width: 300px;">
						  <input type="text" name="config[app_index_bk][<?php echo htmlentities($i); ?>][url]" value="<?php echo htmlentities($app_index_bk[$i]['url']); ?>" placeholder="请输入url/商品:id/功能名称" autocomplete="off" class="layui-input">
						</div>
						<div class="layui-input-inline">
					      <select name="config[app_index_bk][<?php echo htmlentities($i); ?>][type]" >
					        <option value="">类型</option>
					        <?php $_result=config('site.app_func_type');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kk % 2 );++$kk;?>
						    <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
						    <?php endforeach; endif; else: echo "" ;endif; ?>
					      </select>
					    </div>
					    <div class="layui-input-inline">
					      <select name="config[app_index_bk][<?php echo htmlentities($i); ?>][action]">
					        <option value="">app功能</option>
					        <?php $_result=config('site.app_func_action');if(is_array($_result) || $_result instanceof \think\Collection || $_result instanceof \think\Paginator): $kkk = 0; $__LIST__ = $_result;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($kkk % 2 );++$kkk;?>
					        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo['title']); ?></option>
					        <?php endforeach; endif; else: echo "" ;endif; ?>
					      </select>
					    </div>
						<div>
							<span class="upcb">
								<span class="img"><?php if(!(empty($app_index_bk[$i]['img']) || (($app_index_bk[$i]['img'] instanceof \think\Collection || $app_index_bk[$i]['img'] instanceof \think\Paginator ) && $app_index_bk[$i]['img']->isEmpty()))): ?><img src="<?php echo htmlentities(get_img($app_index_bk[$i]['img'])); ?>"><?php endif; ?></span>
								<span class="ipt"><input type="hidden" name="config[app_index_bk][<?php echo htmlentities($i); ?>][img]" value="<?php echo htmlentities($app_index_bk[$i]['img']); ?>"></span>
							</span>
							<button type="button" class="layui-btn up1">
							  <i class="layui-icon">&#xe67c;</i>上传图片
							</button>
						</div>
					</div>
				    <?php } ?>
			    </div>
			  </div>
		  </div>
		</fieldset>
		<hr/>
			<div class="layui-form-item">
			  <div class="layui-input-block">
			    <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form1">立即提交</button>
			  </div>
			</div>
		</form>
 		
     </div>
     <div class="layui-tab-item">
     	<form class="layui-form form2" action="<?php echo url('set/edit'); ?>">
     		<div class="layui-form-item">
			    <label class="layui-form-label">app下载地址</label>
			    <div class="layui-input-block" style="width: 500px;">
			      <input type="text" name="config[app_down_url]" value="<?php echo htmlentities($config['app_down_url']); ?>"  placeholder="" autocomplete="off" class="layui-input">
			    </div>
			</div>
     		<div class="layui-form-item">
			    <label class="layui-form-label">客服链接</label>
			    <div class="layui-input-block" style="width: 400px;">
			      <input type="text" name="config[app_kf_url]" value="<?php echo htmlentities($config['app_kf_url']); ?>"  placeholder="" autocomplete="off" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">新手教程链接</label>
			    <div class="layui-input-block" style="width: 400px;">
			      <input type="text" name="config[app_xsjc_url]" value="<?php echo htmlentities($config['app_xsjc_url']); ?>"  placeholder="" autocomplete="off" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">帮助中心链接</label>
			    <div class="layui-input-block" style="width: 500px;">
			      <input type="text" name="config[app_help_url]" value="<?php echo htmlentities($config['app_help_url']); ?>"    placeholder="是链接哟" autocomplete="off" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">代理特权[标题]</label>
			    <div class="layui-input-block" style="width: 300px;">
			      <input type="text" name="config[app_center_ad][title]" value="<?php echo htmlentities($app_center_ad['title']); ?>"    autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">*标题为空不显示</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">代理特权[内容]</label>
			    <div class="layui-input-block" style="width: 500px;">
			      <input type="text" name="config[app_center_ad][con]" value="<?php echo htmlentities($app_center_ad['con']); ?>"     autocomplete="off" class="layui-input">
			    </div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">代理特权介绍上半部分</label>
			    <div class="layui-input-block" style="width: 80px;">
			      <input type="text" name="config[app_center_ad][intro_top]" value="<?php echo htmlentities($app_center_ad['intro_top']); ?>"    autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">*文章id</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">代理特权介绍下半部分</label>
			    <div class="layui-input-block" style="width: 80px;">
			      <input type="text" name="config[app_center_ad][intro_bottom]" value="<?php echo htmlentities($app_center_ad['intro_bottom']); ?>"    autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">*文章id</div>
			</div>
			<hr>
			<div class="layui-form-item">
			    <label class="layui-form-label">用户隐私协议[url]</label>
			    <div class="layui-input-block" style="width: 500px;">
			      <input type="text" name="config[app_user_protocol]" value="<?php echo htmlentities($config['app_user_protocol']); ?>"     autocomplete="off" class="layui-input">
			    </div>
			</div>

			<fieldset class="layui-elem-field">
			  <legend>搜券教程</legend>
			  <div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">标题搜索</label>
				    <div class="layui-input-block" style="width: 200px;">
				      <input type="number" name="config[app_souquan_help][1]" value="<?php echo htmlentities($app_souquan_help['1']); ?>" placeholder="文章的id,不是网址" autocomplete="off" class="layui-input">
				    </div>
				</div>
			  </div>
			  <div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">赚钱教程</label>
				    <div class="layui-input-block" style="width: 200px;">
				      <input type="number" name="config[app_souquan_help][2]" value="<?php echo htmlentities($app_souquan_help['2']); ?>" placeholder="文章的id,不是网址" autocomplete="off" class="layui-input">
				    </div>
				</div>
			  </div>
			  <div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">新手必看</label>
				    <div class="layui-input-block" style="width: 200px;">
				      <input type="number" name="config[app_souquan_help][3]" value="<?php echo htmlentities($app_souquan_help['3']); ?>" placeholder="文章的id,不是网址" autocomplete="off" class="layui-input">
				    </div>
				</div>
			  </div>
			</fieldset>

     		<hr/>
			<div class="layui-form-item">
              <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form2">立即提交</button>
              </div>
            </div>
     	</form>
     </div>
     <div class="layui-tab-item">
		<form class="layui-form form3" action="<?php echo url('set/edit'); ?>">
			<div class="layui-form-item">
			    <label class="layui-form-label">苹果审核开关</label>
			    <div class="layui-input-inline" >
			      <select name="config[app_apple_examine][is_open]" lay-verify="required">
			        <option value=""></option>
			        <option value="0">关闭</option>
			        <option value="1">开启</option>
			      </select>
			    </div>
			</div>
			<div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">苹果审核版本号</label>
				    <div class="layui-input-inline">
				      <input type="text" name="config[app_apple_examine][version]" value="<?php echo htmlentities($app_apple_examine['version']); ?>" placeholder="版本号" autocomplete="off" class="layui-input">
				    </div>
				</div>
			  </div>
	     	<hr/>
			<div class="layui-form-item">
	          <div class="layui-input-block">
	            <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form3">立即提交</button>
	          </div>
	        </div>
        </form>
     </div>
     <div class="layui-tab-item">
		<form class="layui-form form4" action="<?php echo url('set/edit'); ?>">
			<div class="layui-form-item">
			    <label class="layui-form-label">微信登录开关</label>
			    <div class="layui-input-inline" >
			      <select name="config[app_wx_login]" lay-verify="required">
			        <option value=""></option>
			        <option value="0">关闭</option>
			        <option value="1">开启</option>
			      </select>
			    </div>
			</div>
	     	<hr/>
			<div class="layui-form-item">
	          <div class="layui-input-block">
	            <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form4">立即提交</button>
	          </div>
	        </div>
        </form>
     </div>
     <div class="layui-tab-item">
		<form class="layui-form form5" action="<?php echo url('set/edit'); ?>">
			<div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">appkey:</label>
				    <div class="layui-input-inline" style="width: 300px;">
				      <input type="text" name="config[app_zhichi][appkey]" value="<?php echo htmlentities($config['app_zhichi']['appkey']); ?>" placeholder="" autocomplete="off" class="layui-input">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*更多配置等待扩展</div>
				</div>

			</div>

	     	<hr/>
			<div class="layui-form-item">
	          <div class="layui-input-block">
	            <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form5">立即提交</button>
	          </div>
	        </div>
        </form>
     </div>
     <div class="layui-tab-item">
		<form class="layui-form form5" action="<?php echo url('set/edit'); ?>">
			<div class="layui-field-box">
			  	<div class="layui-form-item">
				    <label class="layui-form-label">appkey:</label>
				    <div class="layui-input-inline" style="width: 300px;">
				      <input type="text" name="config[app_duiba][appkey]" value="<?php echo htmlentities($config['app_duiba']['appkey']); ?>" placeholder="" autocomplete="off" class="layui-input">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*http://hd.dlp.duiba.com.cn/static/index?appId=54988#/setting/sub/appInfo</div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">appsecret:</label>
				    <div class="layui-input-inline" style="width: 300px;">
				      <input type="text" name="config[app_duiba][appsecret]" value="<?php echo htmlentities($config['app_duiba']['appsecret']); ?>" placeholder="" autocomplete="off" class="layui-input">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*http://hd.dlp.duiba.com.cn/static/index?appId=54988#/setting/sub/appInfo</div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">签到链接:</label>
				    <div class="layui-input-inline" style="width: 80%;">
				      <input type="text" name="config[app_duiba][sign_url]" value="<?php echo htmlentities($config['app_duiba']['sign_url']); ?>" autocomplete="off" class="layui-input" >
				    </div>
				    <div class="layui-form-mid layui-word-aux">*只取dbredirect参数</div>
				</div>
				<!-- <div class="layui-form-item">
				    <label class="layui-form-label">商城链接:</label>
				    <div class="layui-input-inline" style="width: 80%;">
				      <input type="text" name="config[app_duiba][mall_url]" value="<?php echo htmlentities($config['app_duiba']['mall_url']); ?>" autocomplete="off" class="layui-input" disabled="">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*只取dbredirect参数</div>
				</div> -->
				<div class="layui-form-item">
				    <label class="layui-form-label">免登录地址:</label>
				    <div class="layui-input-inline" style="width: 80%;">
				      <input type="text" value="<?php echo config('site.domain'); ?>/api/user/duiba_autologin" placeholder="" autocomplete="off" class="layui-input" disabled="">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*请填入兑吧后台,可不填</div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">扣除积分url:</label>
				    <div class="layui-input-inline" style="width: 80%;">
				      <input type="text" value="<?php echo config('site.domain'); ?>/api/duiba/score_dec" placeholder="" autocomplete="off" class="layui-input" disabled="">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*请填入兑吧后台</div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">增加积分url:</label>
				    <div class="layui-input-inline" style="width: 80%;">
				      <input type="text" value="<?php echo config('site.domain'); ?>/api/duiba/score_inc" placeholder="" autocomplete="off" class="layui-input" disabled="">
				    </div>
				    <div class="layui-form-mid layui-word-aux">*请填入兑吧后台</div>
				</div>

			</div>

	     	<hr/>
			<div class="layui-form-item">
	          <div class="layui-input-block">
	            <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form5">立即提交</button>
	          </div>
	        </div>
        </form>
     </div>

  </div>
</div> 


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool','element','upload'],function(){
	var $ = layui.jquery,layer = layui.layer,form = layui.form,tool = layui.tool,element = layui.element,upload = layui.upload;
	//Hash地址的定位
	var layid = location.hash.replace(/^#test=/, '');
	element.tabChange('test', layid);
	element.on('tab(test)', function(elem){
		location.hash = 'test='+ $(this).attr('lay-id');
	});
	//上传图片-app图
	var uploadInst = upload.render({
		elem: '.up1' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,done: function(res){
		  //上传完毕回调
		  var item = this.item;
		  if (res.code==1) {
		  		$(item).prev('.upcb').children('.img').html('<img src="'+res.data.url+'"/>')
		  		$(item).prev('.upcb').find('.ipt input').val(res.data.id);
		    	//$('#iptiab').val(res.data.id);
		    	//$('.appheadimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	//
	<?php if(is_array($app_index_banner) || $app_index_banner instanceof \think\Collection || $app_index_banner instanceof \think\Paginator): $i = 0; $__LIST__ = $app_index_banner;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
	tool.setValue('config[app_index_banner][<?php echo htmlentities($i-1); ?>][type]','<?php echo htmlentities($vo['type']); ?>');
	tool.setValue('config[app_index_banner][<?php echo htmlentities($i-1); ?>][action]','<?php echo htmlentities($vo['action']); ?>');
	<?php endforeach; endif; else: echo "" ;endif; if(is_array($app_index_icon) || $app_index_icon instanceof \think\Collection || $app_index_icon instanceof \think\Paginator): $i = 0; $__LIST__ = $app_index_icon;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
	tool.setValue('config[app_index_icon][<?php echo htmlentities($i-1); ?>][type]','<?php echo htmlentities($vo['type']); ?>');
	tool.setValue('config[app_index_icon][<?php echo htmlentities($i-1); ?>][action]','<?php echo htmlentities($vo['action']); ?>');
	<?php endforeach; endif; else: echo "" ;endif; if(is_array($app_index_bk) || $app_index_bk instanceof \think\Collection || $app_index_bk instanceof \think\Paginator): $i = 0; $__LIST__ = $app_index_bk;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
	tool.setValue('config[app_index_bk][<?php echo htmlentities($i-1); ?>][type]','<?php echo htmlentities($vo['type']); ?>');
	tool.setValue('config[app_index_bk][<?php echo htmlentities($i-1); ?>][action]','<?php echo htmlentities($vo['action']); ?>');
	<?php endforeach; endif; else: echo "" ;endif; ?>
	tool.setValue('config[app_apple_examine][is_open]','<?php echo htmlentities($app_apple_examine['is_open']); ?>');
	tool.setValue('config[app_wx_login]','<?php echo htmlentities($config['app_wx_login']); ?>');
	tool.setValue('config[app_goods_open_type]','<?php echo htmlentities($config['app_goods_open_type']); ?>');
});
</script>

</html>