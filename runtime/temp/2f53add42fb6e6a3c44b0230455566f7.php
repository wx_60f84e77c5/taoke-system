<?php /*a:2:{s:79:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/good/section/edit.html";i:1546095382;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<form class="layui-form"> 
	<div class="layui-form-item">
	    <label class="layui-form-label">版块名称</label>
	    <div class="layui-input-inline">
	      <input type="text" name="name" value="<?php echo htmlentities($info['name']); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required">
	    </div>
	</div>
  <div class="layui-form-item">
      <label class="layui-form-label">显示到精品推荐</label>
      <div class="layui-input-block">
        <input type="radio" name="is_jinping" value="1" title="显示">
        <input type="radio" name="is_jinping" value="0" title="不显示" checked>
      </div>
  </div>
  <div class="layui-form-item">
      <label class="layui-form-label">显示到首页</label>
      <div class="layui-input-block">
        <input type="radio" name="is_index" value="1" title="显示">
        <input type="radio" name="is_index" value="0" title="不显示" checked>
      </div>
  </div>
  <div class="layui-form-item">
      <label class="layui-form-label">过滤佣金</label>
      <div class="layui-input-inline">
        <input type="number" name="filter_yj" value="<?php echo htmlentities((isset($info['filter_yj']) && ($info['filter_yj'] !== '')?$info['filter_yj']:10)); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required"> 
      </div>
      <div class="layui-form-mid layui-word-aux">%百分比</div>
  </div>
  <div class="layui-form-item">
      <label class="layui-form-label">过滤销量</label>
      <div class="layui-input-inline">
        <input type="number" name="filter_volume" value="<?php echo htmlentities((isset($info['filter_volume']) && ($info['filter_volume'] !== '')?$info['filter_volume']:0)); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required"> 
      </div>
      <div class="layui-form-mid layui-word-aux"></div>
  </div>
  <div class="layui-form-item">
      <label class="layui-form-label">店铺类型</label>
      <div class="layui-input-block">
        <input type="radio" name="user_type" value="1" title="淘宝">
        <input type="radio" name="user_type" value="2" title="天猫">
        <input type="radio" name="user_type" value="0" title="不限" checked>
      </div>
  </div>

  <div class="layui-form-item">
    <label class="layui-form-label">商品排序方式</label>
    <div class="layui-input-block">
      <select name="sort_type" lay-verify="required">
          <option></option>
          <optgroup label="按添加时间"></optgroup>
          <option value="id desc" selected="">按添加时间排前</option>
          <option value="id asc">按添加时间排后</option>
          <optgroup label="按销量"></optgroup>
          <option value="sales desc">销量从高到低</option>
          <option value="sales asc">销量从低到高</option>
          <optgroup label="按价格-券后价"></optgroup>
          <option value="price_after_quan desc">价格从高到低</option>
          <option value="price_after_quan asc">价格从低到高</option>
          <optgroup label="佣金比例"></optgroup>
          <option value="rate desc">佣金从高到低</option>
          <option value="rate asc">佣金从低到高</option>
          <optgroup label="按券开始时间"></optgroup>
          <option value="quan_start_time desc">按券开始时间排前</option>
          <option value="quan_start_time asc">按券开始时间排后</option>
          <optgroup label="随机"></optgroup>
          <option value="rand()">随机排序</option>
      </select>
    </div>
  </div>

  <div class="layui-form-item">
      <label class="layui-form-label">版块排序</label>
      <div class="layui-input-inline">
        <input type="sort" name="sort" value="<?php echo htmlentities((isset($info['sort']) && ($info['sort'] !== '')?$info['sort']:0)); ?>" placeholder="" autocomplete="off" class="layui-input" lay-verify="required"> 
      </div>
      <div class="layui-form-mid layui-word-aux">数字越大越靠前</div>
  </div>
  
  <div class="layui-form-item layui-sumbtn">
	  <div class="layui-input-block">
	    <button class="layui-btn self_window" lay-submit="" lay-filter="ajax-post" type="submit" target-form="layui-form">立即提交</button>
	    <button type="reset" class="layui-btn layui-btn-primary">重置</button>
	  </div>
  </div>
  
</form>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool'],function(){
	var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool;
	tool.setValue('is_jinping','<?php echo htmlentities($info['is_jinping']); ?>');
  tool.setValue('is_index','<?php echo htmlentities($info['is_index']); ?>');
  tool.setValue('user_type','<?php echo htmlentities($info['user_type']); ?>');
  tool.setValue('sort_type','<?php echo htmlentities($info['sort_type']); ?>');
});
</script>

</html>