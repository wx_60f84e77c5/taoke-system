<?php /*a:2:{s:78:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/user/index/index.html";i:1547509564;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
</head>

<body>
	<div class="admin-body">
		
<blockquote class="layui-elem-quote">
	<a href="javascript:location.reload();" class="layui-btn layui-btn-sm"><i class="layui-icon">&#x1002;</i></a>
	<button url="<?php echo url('del'); ?>" class="layui-btn layui-btn-sm confirm" lay-submit lay-filter="ajax-post"  target-form="ids" >
		<i class="layui-icon">&#xe640;</i> 删除
	</button>
	<form class="layui-form search" action="" style="display: inline-block;float: right;" _lpchecked="1">
		<div class="layui-input-inline" style="width: 300px;">
			<input type="text" name="date" class="layui-input" id="date" value="<?php echo date('Y-m-d 00:00:00',time()); ?> ~ <?php echo date('Y-m-d 23:59:59',time()); ?>">
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 120px;">
				<select name="leval" >
			        <option value="">会员等级</option>
			        <?php if(is_array($leval_arr) || $leval_arr instanceof \think\Collection || $leval_arr instanceof \think\Paginator): $i = 0; $__LIST__ = $leval_arr;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?>
			        <option value="<?php echo htmlentities($key); ?>"><?php echo htmlentities($vo); ?></option>
			        <?php endforeach; endif; else: echo "" ;endif; ?>
			    </select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline" style="width: 90px;">
				<select name="status">
					<option value="-1"></option>
			        <option value="1">正常</option>
			        <option value="0">冻结</option>
			     </select>
			</div>
		</div>
		<div class="layui-inline">
			<div class="layui-input-inline">
				<input type="text" name="keyword" placeholder="ID/手机/昵称/邀请码" autocomplete="off" class="layui-input">
			</div>
		</div>
		 <div class="layui-inline">
		 	<div class="layui-input-inline">
		 		<button class="layui-btn layui-btn-sm sbtn" lay-submit="" lay-filter="searchsub" id="search"><i class="layui-icon"></i> 搜索</button>
		 		<button class="layui-btn layui-btn-sm layui-btn-danger"  id="export"><i class="fa fa-file-excel-o" aria-hidden="true"></i> 导出</button>
		 	</div>
		 </div>
	</form>
</blockquote>
<table id="tb1" lay-filter="_tb1"></table>

	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/html" id="check">
	<input type="checkbox" lay-skin="primary" name="ids[]" class="ids" value="{{ d.uid }}">
</script>
<script type="text/html" id="bar">
	<div class="layui-btn-group">
	  <a class="layui-btn layui-btn-xs edit" data-url="<?php echo url('edit'); ?>?ids={{ d.uid }}" >编辑</a>
	  <a class="layui-btn layui-btn-xs confirm_del" lay-event="del" data-url="<?php echo url('del'); ?>?ids={{ d.uid }}" >删除</a>
	  <a class="layui-btn layui-btn-xs frozen" data-url="<?php echo url('frozen'); ?>?ids={{ d.uid }}" >冻结</a>
	</div>
</script>
<script type="text/html" id="nickname">
	<span class="layui-text"><a href="javascript:;" class="show_userinfo"  data-title="【{{d.nickname}}】的用户信息" data-url="<?php echo url('info'); ?>?id={{d.uid}}" >{{d.nickname}}</a></span>
</script>
<script type="text/html" id="pid_nickname">
	<span class="layui-text"><a href="javascript:;" class="show_userinfo"  data-title="【{{d.pid_nickname}}】的用户信息" data-url="<?php echo url('info'); ?>?id={{d.pid}}" >[{{d.pid}}]{{d.pid_nickname}}</a></span>
</script>
<script type="text/html" id="ppid">
	<span class="layui-text"><a href="javascript:;" class="show_userinfo"  data-title="【{{d.ppid}}】的用户信息" data-url="<?php echo url('info'); ?>?id={{d.ppid}}" >[{{d.ppid}}]{{d.ppname}}</a></span>
</script>
<script>
	layui.config({
		base: '/static/js/',
	});
	layui.use(['tool','laydate'], function() {
		var $ = layui.$,layer = layui.layer, form = layui.form,table = layui.table,tool = layui.tool,laydate=layui.laydate;
		tool.show_userinfo();
		var tableobj = table.render({
			elem:'#tb1',
			url:'<?php echo url('index'); ?>',
			limit:15,
			limits:[10,15,20,50,100],
			page:true,
			//size:'sm',
			method:'get',
			height:'full-100',
			cols:[[
				{title:'<input type=checkbox lay-filter=allChoose lay-skin=primary>',fixed:'left',templet:'#check',width:50},
				{title:'ID',field:'uid',width:80},
				{title:'昵称',field:'nickname',templet:'#nickname'},
				{title:'推荐人',field:'pid_nickname',templet:'#pid_nickname',minWidth:180},
				{title:'上上级',field:'ppid',templet:'#ppid'},
				{title:'手机',field:'mobile',width:120},
				{title:'余额',field:'money',width:100,sort:true},
				{title:'积分',field:'score',width:60},
				{title:'经验值',field:'experience',width:80},
				{title:'会员等级',field:'leval',width:90},
				{title:'最后登录设备',field:'last_device_model'},
				{title:'注册时间',field:'create_time',width:165},
				{title:'邀请码',field:'invite_code'},
				{title:'状态',field:'status_text'},
				{title:'操作',fixed: 'right', width:150, align:'center', templet: '#bar'}

			]]
		});
		//添加
		$(document).on('click','#add,.edit',function(){
		    var url = '<?php echo url('add'); ?>',title = '添加';
		    if($(this).hasClass('edit')){
		      url = $(this).data('url');
		      title = '编辑';
		    }
		  	layer.open({
		      title:title,
		      type: 2,
		      area: ['50%', '80%'],
		      fixed: false, //不固定
		      maxmin: true,
		      content: url,
		      shade:0
		    });
		});
		//搜索
		form.on('submit(searchsub)',function(data){
			var fields = $(data.form).serialize();
			tableobj.reload({
				where:data.field
				,page: {curr: 1 }
			});
			return false;
		})
		//冻结
		$(document).on('click','.frozen',function(){
			var that = this;
			layer.confirm('确认要冻结吗？', {
			  title:'提示'
			},function(){
				$.get($(that).data('url'),function(ret){
					if (ret.code==1) {
			    		layer.msg('操作成功',{icon:1});
			    		location.reload();
			    	}else{
			    		layer.msg(ret.msg||'异常~',{icon:2});
			    	}
				});
				
			});
		});
		//排序
		table.on('sort(_tb1)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
		  //console.log(obj.field); //当前排序的字段名
		  //console.log(obj.type); //当前排序类型：desc（降序）、asc（升序）、null（空对象，默认排序）
		  //console.log(this); //当前排序的 th 对象
		  
		  //尽管我们的 table 自带排序功能，但并没有请求服务端。
		  //有些时候，你可能需要根据当前排序的字段，重新向服务端发送请求，从而实现服务端排序，如：
		  tableobj.reload({
		    initSort: obj //记录初始排序，如果不设的话，将无法标记表头的排序状态。 layui 2.1.1 新增参数
		    ,where: { //请求参数（注意：这里面的参数可任意定义，并非下面固定的格式）
		      //field: obj.field //排序字段
		      //,order: obj.type //排序方式
		      order:obj.field+' '+obj.type
		    }
		  });
		});
		//日期时间范围
		laydate.render({
		  elem: '#date'
		  ,type: 'datetime'
		  ,range: '~'
		  ,min:'2018-10-01 00:00:00'
		  ,max:'<?php echo date('Y-m-d 23:59:59',time()); ?>'
		});
		//export
		$('#export').click(function(){
			var url = '<?php echo url('export',['type'=>'user']); ?>?';
			var ids = $('.ids:checked').serialize();
			var form = $('form').serialize();
			location.href = url+'&'+form+'&'+ids;

			return false;
		});
	});
</script>

</html>