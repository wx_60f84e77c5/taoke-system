<?php /*a:2:{s:76:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/user/set/index.html";i:1547509564;s:72:"/www/wwwroot/demo-dev.taokeyun.cn/application/admin/view/index/base.html";i:1546095382;}*/ ?>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<title><?php if(empty($title) || (($title instanceof \think\Collection || $title instanceof \think\Paginator ) && $title->isEmpty())): ?><?php echo htmlentities($site['name']); else: ?><?php echo htmlentities($title); ?>-<?php echo htmlentities($site['name']); ?><?php endif; ?></title>
	<link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css" />
	<link rel="stylesheet" type="text/css" href="/static/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="/static/css/common.css" />
	
<style type="text/css">
/*.layui-form-label{width: 150px;}*/
.qr img{width: 200px;height: 200px;position: absolute;bottom: 150px;left: 170px;}
</style>

</head>

<body>
	<div class="admin-body">
		

<div class="layui-tab layui-tab-brief" lay-filter="test">
  <ul class="layui-tab-title">
    <li class="layui-this"  lay-id="11">模式规则</li>
    <li  lay-id="22">邀请好友设置</li>
    <li  lay-id="33">提现设置</li>
  </ul>
  <div class="layui-tab-content" style="height: 100px;">
    <div class="layui-tab-item layui-show">
    	<form class="layui-form" action="">
    	<div class="layui-form-item">
		    <label class="layui-form-label">等级名称:</label>
		    <div class="layui-input-block" style="width: 60%;">
			    <table class="layui-table">
			    	<thead>
			    		<tr>
				    		<th>第0级别</th>
				    		<th>第1级别</th>
				    		<th>第2级别</th>
				    		<th>第3级别</th>
				    	</tr>
			    	</thead>
			    	<tr>
			    		<td><input type="text" name="lvcfg[name][0]" value="<?php echo htmlentities((isset($lvcfg['name']['0']) && ($lvcfg['name']['0'] !== '')?$lvcfg['name']['0']:"")); ?>" class="layui-input"></td>
			    		<td><input type="text" name="lvcfg[name][1]" value="<?php echo htmlentities((isset($lvcfg['name']['1']) && ($lvcfg['name']['1'] !== '')?$lvcfg['name']['1']:"")); ?>" class="layui-input"></td>
			    		<td><input type="text" name="lvcfg[name][2]" value="<?php echo htmlentities((isset($lvcfg['name']['2']) && ($lvcfg['name']['2'] !== '')?$lvcfg['name']['2']:"")); ?>" class="layui-input"></td>
			    		<td><input type="text" name="lvcfg[name][3]" value="<?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"")); ?>" class="layui-input"></td>
			    	</tr>
			    </table>
			</div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">手续费(%)</label>
		    <div class="layui-input-inline" style="width: 100px;">
		      <input type="text" name="lvcfg[kengdie_fee]" value="<?php echo htmlentities($lvcfg['kengdie_fee']); ?>" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
		    </div>
		    <div class="layui-form-mid layui-word-aux">*例阿里技术服务费率，若填写10，则取分佣的90%作为100%分</div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">订单返利方式</label>
		    <div class="layui-input-inline" style="width: 200px;">
			    <select class="layui-input" name="lvcfg[order_jiesuan_type]">
			      	<option value="0">按每月某天</option>
			      	<option value="1">按订单结算后多少天</option>
			    </select>
		    </div>
		    <div class="layui-form-mid layui-word-aux"></div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">订单返利时间</label>
		    <div class="layui-input-inline" style="width: 80px;">
		      <input type="text" name="lvcfg[order_jiesuan_time]" value="<?php echo htmlentities($lvcfg['order_jiesuan_time']); ?>" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
		    </div>
		    <div class="layui-form-mid layui-word-aux">*如果是返利方式是每月某天则此天结算上月的订单给用户，反之订单结算n天后，分佣到用户账户(可提现)</div>
		</div>
		<div class="">
		    <label class="layui-form-label">购物分佣规则:</label>
		    <div class="layui-input-block" style="width: 70%;">
			    <table class="layui-table">
			    	<thead>
			    		<tr>
				    		<th>行为</th>
				    		<th><?php echo htmlentities((isset($lvcfg['name']['0']) && ($lvcfg['name']['0'] !== '')?$lvcfg['name']['0']:"第0级别")); ?></th>
				    		<th><?php echo htmlentities((isset($lvcfg['name']['1']) && ($lvcfg['name']['1'] !== '')?$lvcfg['name']['1']:"第1级别")); ?></th>
				    		<th><?php echo htmlentities((isset($lvcfg['name']['2']) && ($lvcfg['name']['2'] !== '')?$lvcfg['name']['2']:"第2级别")); ?></th>
				    		<th><?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"第3级别")); ?></th>
				    	</tr>
			    	</thead>
			    	<tbody>
			    		<tr>
				    		<td>升级条件(自动升级)</td>
				    		<td>下载注册即是</td>
				    		<td>
				    			1.购买:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][1][1]" value="<?php echo htmlentities($lvcfg['leval_condition']['1']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								元礼包，或
								<br/><br/>
								2.付费:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][1][2]" value="<?php echo htmlentities($lvcfg['leval_condition']['1']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								/月
				    		</td>
				    		<td>
				    			1.直推<?php echo htmlentities((isset($lvcfg['name']['1']) && ($lvcfg['name']['1'] !== '')?$lvcfg['name']['1']:"第1级别")); ?>:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][2][1]" value="<?php echo htmlentities($lvcfg['leval_condition']['2']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								人，或
								<br/><br/>
								2.付费:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][2][2]" value="<?php echo htmlentities($lvcfg['leval_condition']['2']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								/年(365天)
				    		</td>
				    		<td>
				    			1.直推<?php echo htmlentities((isset($lvcfg['name']['2']) && ($lvcfg['name']['2'] !== '')?$lvcfg['name']['2']:"第2级别")); ?>:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][3][1]" value="<?php echo htmlentities($lvcfg['leval_condition']['3']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								人，或
								<br/><br/>
								2.付费:
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[leval_condition][3][2]" value="<?php echo htmlentities($lvcfg['leval_condition']['3']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div>
								/年(365天)
				    		</td>
				    	</tr>
				    	<thead><tr><th colspan="5" style="text-align: center;color: #ff0000;">会员付费部分</th></tr></thead>
				    	<tr>
				    		<td>月付管理费奖<br/><span style="text-decoration: line-through;">(上级>=下级的等级时才得钱)</span></td>
				    		<td>无</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][1][1]" value="<?php echo htmlentities($lvcfg['yuefu']['1']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][1][2]" value="<?php echo htmlentities($lvcfg['yuefu']['1']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
				    		</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][2][1]" value="<?php echo htmlentities($lvcfg['yuefu']['2']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][2][2]" value="<?php echo htmlentities($lvcfg['yuefu']['2']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
				    		</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][3][1]" value="<?php echo htmlentities($lvcfg['yuefu']['3']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[yuefu][3][2]" value="<?php echo htmlentities($lvcfg['yuefu']['3']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> %
				    		</td>
				    	</tr>
				    	<tr>
				    		<td>礼包推荐奖<br/>(上级>=下级的等级时才得钱)</td>
				    		<td>无</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][1][1]" value="<?php echo htmlentities($lvcfg['libao']['1']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][1][2]" value="<?php echo htmlentities($lvcfg['libao']['1']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
				    		</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][2][1]" value="<?php echo htmlentities($lvcfg['libao']['2']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][2][2]" value="<?php echo htmlentities($lvcfg['libao']['2']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
				    		</td>
				    		<td>
				    			一级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][3][1]" value="<?php echo htmlentities($lvcfg['libao']['3']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
								<br/><br/>
								二级推荐
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 100px;">
								      <input type="number" name="lvcfg[libao][3][2]" value="<?php echo htmlentities($lvcfg['libao']['3']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">
								    </div>
								</div> 元
				    		</td>
				    	</tr>
				    	<thead><tr><th colspan="5" style="text-align: center;color: #ff0000;">以下为购物部分</th></tr></thead>
				    	<tr>
				    		<td>自购返佣(%)</td>
				    		<td><input type="number" name="lvcfg[zigou][0]" value="<?php echo htmlentities($lvcfg['zigou']['0']); ?>"  placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[zigou][1]" value="<?php echo htmlentities($lvcfg['zigou']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[zigou][2]" value="<?php echo htmlentities($lvcfg['zigou']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[zigou][3]" value="<?php echo htmlentities($lvcfg['zigou']['3']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    	</tr>
				    	<tr>
				    		<td>自己分享返佣(%)</td>
				    		<td><input type="number" name="lvcfg[share][0]" value="<?php echo htmlentities($lvcfg['share']['0']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[share][1]" value="<?php echo htmlentities($lvcfg['share']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[share][2]" value="<?php echo htmlentities($lvcfg['share']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[share][3]" value="<?php echo htmlentities($lvcfg['share']['3']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    	</tr>
				    	<tr>
				    		<td>下属<?php echo htmlentities((isset($lvcfg['name']['0']) && ($lvcfg['name']['0'] !== '')?$lvcfg['name']['0']:"第0级别")); ?>,购物奖励(%)</td>
				    		<td>无奖励</td>
				    		<td><input type="number" name="lvcfg[0gouwu][1]" value="<?php echo htmlentities($lvcfg['0gouwu']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[0gouwu][2]" value="<?php echo htmlentities($lvcfg['0gouwu']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td> <td><input type="number" name="lvcfg[0gouwu][3]" value="<?php echo htmlentities($lvcfg['0gouwu']['3']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    	</tr>
				    	<tr>
				    		<td>下属<?php echo htmlentities((isset($lvcfg['name']['0']) && ($lvcfg['name']['0'] !== '')?$lvcfg['name']['0']:"第0级别")); ?>,分享奖励(%)</td>
				    		<td>无奖励</td>
				    		<td><input type="number" name="lvcfg[0share][1]" value="<?php echo htmlentities($lvcfg['0share']['1']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    		<td><input type="number" name="lvcfg[0share][2]" value="<?php echo htmlentities($lvcfg['0share']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td> <td><input type="number" name="lvcfg[0share][3]" value="<?php echo htmlentities($lvcfg['0share']['3']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input"></td>
				    	</tr>
				    	<tr>
				    		<td>N级收益(%,购物与分享)</td>
				    		<td>无</td>
				    		<td>无</td>
				    		<td><input type="number" name="lvcfg[ngouwu][2]" value="<?php echo htmlentities($lvcfg['ngouwu']['2']); ?>" placeholder="0不生效" autocomplete="off" class="layui-input">%</td>
				    		<td>
				    			第上一<?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"第3级别")); ?>奖：
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[ngouwu][3][1]" value="<?php echo htmlentities((isset($lvcfg['ngouwu']['3']['1']) && ($lvcfg['ngouwu']['3']['1'] !== '')?$lvcfg['ngouwu']['3']['1']:"0")); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div> %
								<br/>
								第上二<?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"第3级别")); ?>奖：
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[ngouwu][3][2]" value="<?php echo htmlentities((isset($lvcfg['ngouwu']['3']['2']) && ($lvcfg['ngouwu']['3']['2'] !== '')?$lvcfg['ngouwu']['3']['2']:"0")); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div> %
								<br/>
								第上三<?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"第3级别")); ?>奖：
				    			<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[ngouwu][3][3]" value="<?php echo htmlentities((isset($lvcfg['ngouwu']['3']['3']) && ($lvcfg['ngouwu']['3']['3'] !== '')?$lvcfg['ngouwu']['3']['3']:"0")); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div> %
				    		</td>
				    	</tr>
				    	<tr>
				    		<td colspan="5">
				    			注：<br/>
				    			1.除了<?php echo htmlentities((isset($lvcfg['name']['3']) && ($lvcfg['name']['3'] !== '')?$lvcfg['name']['3']:"第3级别")); ?>外，其它等级平级不分润（购物）<br/>
				    			2.<?php echo htmlentities((isset($lvcfg['name']['2']) && ($lvcfg['name']['2'] !== '')?$lvcfg['name']['2']:"第2级别")); ?>自购时不会再次额外抽取（购物）
				    		</td>
				    	</tr>
			    	</tbody>
			    	
			    </table>
			</div>
		</div>
		
		<h2 style="text-align: center;">积分相关设置</h2>
		<hr/>
		<div class="layui-form-item">
		    <label class="layui-form-label">购买代理赠：</label>
		    送付款金额*
		    <div class="layui-inline" style="width: 60px;">
		    	<input type="number" name="lvcfg[score][uplv]" class="layui-input" value="<?php echo htmlentities($lvcfg['score']['uplv']); ?>">
		    </div>
		    倍的积分
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">邀请好友：</label>
		    <div class="layui-input-inline" style="width: 100px;">
		      <input type="text" name="lvcfg[score][invite]" value="<?php echo htmlentities($lvcfg['score']['invite']); ?>" required  lay-verify="required" placeholder="请输入" autocomplete="off" class="layui-input">
		    </div>
		    <div class="layui-form-mid layui-word-aux">*每邀请一位好友送多少积分,0为不送</div>
		</div>
		<div class="layui-form-item">
		    <label class="layui-form-label">任务：</label>
		    <div class="layui-input-block" style="width: 50%;">
			    <table class="layui-table">
			    	<thead>
			    		<tr>
				    		<th>类型</th>
				    		<th>动作</th>
				    		<th>奖励积分</th>
				    	</tr>
			    	</thead>
			    	<tbody>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				购买
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][1][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['1']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								个有效订单
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][1][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['1']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				分享购买
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][2][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['2']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								个有效订单
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][2][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['2']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				完成
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][3][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['3']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								次app分享
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][3][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['3']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				完成
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][4][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['4']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								次商品分享
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][4][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['4']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				邀请
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][5][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['5']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								位有效粉丝
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][5][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['5']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>新手任务</td>
			    			<td>
			    				首次购买会员
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][newuser][6][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['newuser']['6']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>


			    		<tr>
			    			<td>每日任务</td>
			    			<td>
			    				浏览
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][7][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['7']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								件商品
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][7][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['7']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>每日任务</td>
			    			<td>
			    				完成
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][8][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['8']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								次app分享
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][8][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['8']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>每日任务</td>
			    			<td>
			    				完成
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][9][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['9']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								次商品分享
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][9][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['9']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>每日任务</td>
			    			<td>
			    				邀请
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][10][condition]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['10']['condition']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
								位有效粉丝
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][10][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['10']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    		<tr>
			    			<td>每日任务</td>
			    			<td>
			    				邀请好友购买会员
			    			</td>
			    			<td>
			    				<div class="layui-inline">
								    <div class="layui-input-inline" style="width: 80px;">
								      <input type="number" name="lvcfg[score][task][days][11][reward]" value="<?php echo htmlentities($lvcfg['score']['task']['days']['11']['reward']); ?>" placeholder="" autocomplete="off" class="layui-input">
								    </div>
								</div>
			    			</td>
			    		</tr>
			    	</tbody>
			    </table>
			</div>
		</div>
		<hr/>
		<div class="layui-form-item">
          <div class="layui-input-block">
            <button class="layui-btn" type="submit" lay-submit lay-filter="ajax-post"  target-form="layui-form">立即提交保存</button>
          </div>
        </div>
		</form>
		<br/><br/><br/><br/>
     </div>
     <div class="layui-tab-item">
     	<form class="layui-form form1" action="<?php echo url('set/edit'); ?>">
			<div class="layui-form-item">
				<label class="layui-form-label">图片域名</label>
				<div class="layui-input-block" style="width: 200px;">
					<input type="text" name="config[domain_share]" value="<?php echo htmlentities($config['domain_share']); ?>" required  lay-verify="required" placeholder="请输入域名" autocomplete="off" class="layui-input">
				</div>
			</div>
     		<div class="layui-form-item">
			    <label class="layui-form-label">强制邀请码注册</label>
			    <div class="layui-inline" style="width: 200px;">
			    	<select class="layui-input" name="config[invite_need_code]">
				      	<option value="0">关闭</option>
				      	<option value="1">开启</option>
				    </select>
			    </div>
			</div>
     		<div class="layui-form-item">
			    <label class="layui-form-label">APP内邀请图(750*380):</label>
			    <div class="appheadimg layui-inline">
			    	<?php if($config['invite_app_banner'] == '0'): ?>
			     	<img src="/static/images/0.jpg" width="300" />
			     	<?php else: ?>
			     	<img src="<?php echo htmlentities(get_img($config['invite_app_banner'])); ?>"  style="max-width: 300px;max-height: 150px;" />
			     	<?php endif; ?>
			     	<input type="hidden" name="config[invite_app_banner]" value="<?php echo htmlentities($config['invite_app_banner']); ?>" id="iptiab">
			    </div>
			    <button type="button" class="layui-btn layui-btn-small" id="invite_app_banner">
				  <i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">wap页邀请图(750*530):</label>
			    <div class="wapheadimg layui-inline">
			    	<?php if($config['invite_wap_banner'] == '0'): ?>
			     	<img src="/static/images/0.jpg" width="300" />
			     	<?php else: ?>
			     	<img src="<?php echo htmlentities(get_img($config['invite_wap_banner'])); ?>"  style="max-width: 300px;" />
			     	<?php endif; ?>
			     	<input type="hidden" name="config[invite_wap_banner]" value="<?php echo htmlentities($config['invite_wap_banner']); ?>" id="iptiwb">
			    </div>
			    <button type="button" class="layui-btn layui-btn-small" id="invite_wap_banner">
				  <i class="layui-icon">&#xe67c;</i>上传图片
				</button>
			</div>
			<fieldset class="layui-elem-field">
			  <legend>分享链接设置</legend>
			  <div class="layui-field-box">
			    <div class="layui-form-item">
				    <label class="layui-form-label">分享标题</label>
				    <div class="layui-input-block" style="width: 200px;">
				      <input type="text" name="config[invite_share_title]" value="<?php echo htmlentities($config['invite_share_title']); ?>" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
				    </div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">分享内容</label>
				    <div class="layui-input-block" style="width: 500px;">
				      <input type="text" name="config[invite_share_content]" value="<?php echo htmlentities($config['invite_share_content']); ?>" required  lay-verify="required" placeholder="请输入内容" autocomplete="off" class="layui-input">
				    </div>
				</div>
				<div class="layui-form-item">
				    <label class="layui-form-label">分享图片(512*512)</label>
				    <div class="shareimg layui-inline">
				    	<?php if($config['invite_share_img'] == '0'): ?>
				     	<img src="/static/images/0.jpg" width="150" height="150" />
				     	<?php else: ?>
				     	<img src="<?php echo htmlentities(get_img($config['invite_share_img'])); ?>"   width="150" height="150" />
				     	<?php endif; ?>
				     	<input type="hidden" name="config[invite_share_img]" value="<?php echo htmlentities($config['invite_share_img']); ?>" id="iptisi">
				    </div>
				    <button type="button" class="layui-btn layui-btn-small" id="invite_share_img">
					  <i class="layui-icon">&#xe67c;</i>上传图片
					</button>
				</div>
			  </div>
			</fieldset>
			<fieldset class="layui-elem-field">
			  <legend>分享海报设置</legend>
			  <div class="layui-field-box">
				  <div class="layui-row">
				    <div class="layui-col-md5">
				        <div class="layui-form-item">
						    <label class="layui-form-label">背景图(1080*1920)</label>
						    <div class="sharehbimg layui-inline">
						    	<?php if($config['app_share_hb'] == '0'): ?>
						     	<img src="/static/images/0.jpg" width="150" height="150" />
						     	<?php else: ?>
						     	<img src="<?php echo htmlentities(get_img($config['app_share_hb'])); ?>"   width="150" height="150" />
						     	<?php endif; ?>
						     	<input type="hidden" name="config[app_share_hb]" value="<?php echo htmlentities($config['app_share_hb']); ?>" id="iptash">
						    </div>
						    <button type="button" class="layui-btn layui-btn-small" id="app_share_hb">
							  <i class="layui-icon">&#xe67c;</i>上传图片
							</button>
						</div>
				    </div>
				    <div class="layui-col-md7">
				    	<!-- 暂时先不做自定义二维码位置 -->
				      <div class="hb">
				      	 <span class="sharehbimg">
				      	 	<?php if($config['app_share_hb'] == '0'): ?>
					     	<img src="/static/images/hb_default_bg.png"  width="540" height="960" />
					     	<?php else: ?>
					     	<img src="<?php echo htmlentities(get_img($config['app_share_hb'])); ?>"   width="540" height="960" />
					     	<?php endif; ?>
				      	 </span>
				      	 <div class="qr"><img src="/static/images/hb_default_qr.jpg"></div>
				      </div>
				    </div>
				  </div>
			</fieldset>
			
			<hr/>
			<div class="layui-form-item">
              <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form1">立即提交</button>
              </div>
            </div>
     	</form>
     </div>
     <div class="layui-tab-item">
     	<form class="layui-form form2" action="<?php echo url('set/edit'); ?>">
     		<div class="layui-form-item">
			    <label class="layui-form-label">余额提现整数倍</label>
			    <div class="layui-input-inline" style="width: 150px;">
			      <input type="number" name="config[draw_money_min_int]" value="<?php echo htmlentities($config['draw_money_min_int']); ?>" required  lay-verify="required" placeholder="请输入>1金额" autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">*整数倍限制</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">余额提现最小金额</label>
			    <div class="layui-input-inline" style="width: 100px;">
			      <input type="number" name="config[draw_money_min]" value="<?php echo htmlentities($config['draw_money_min']); ?>" required  lay-verify="required" placeholder="请输入>1金额" autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">*多少起提</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">可提现时间</label>
			    <div class="layui-inline" style="width: 60px;">
			    	<select class="layui-input" name="config[draw_money_time_start]">
				      <?php $__FOR_START_646817043__=0;$__FOR_END_646817043__=24;for($i=$__FOR_START_646817043__;$i < $__FOR_END_646817043__;$i+=1){ ?>
				      	<option <?php if($i == $config['draw_money_time_start']): ?>selected<?php endif; ?>><?php echo htmlentities($i); ?></option>
				      <?php } ?>
				    </select>
			    </div>
			    至&nbsp;
			    <div class="layui-inline" style="width: 60px;">
			      <select class="layui-input" name="config[draw_money_time_end]">
				      <?php $__FOR_START_1258350824__=0;$__FOR_END_1258350824__=24;for($i=$__FOR_START_1258350824__;$i < $__FOR_END_1258350824__;$i+=1){ ?>
				      	<option <?php if($i == $config['draw_money_time_end']): ?>selected<?php endif; ?>><?php echo htmlentities($i); ?></option>
				      <?php } ?>
				    </select>
			    </div>
			    点
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">手续费</label>
			    <div class="layui-input-inline" style="width: 50px;">
			      <input type="number" name="config[app_draw_per]" value="<?php echo htmlentities($config['app_draw_per']); ?>" required  lay-verify="required"  autocomplete="off" class="layui-input">
			    </div>
			    <div class="layui-form-mid layui-word-aux">%，0-100</div>
			</div>
			<div class="layui-form-item">
			    <label class="layui-form-label">提现说明</label>
			    <div class="layui-input-inline" style="width: 400px;">
			      <textarea name="config[draw_intro]" class="layui-textarea" rows="10"><?php echo htmlentities($config['draw_intro']); ?></textarea>
			    </div>
			</div>
			<hr/>
			<div class="layui-form-item">
              <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-submit="" lay-filter="ajax-post" type="submit" target-form="form2">立即提交</button>
              </div>
            </div>
     	</form>
     </div>
  </div>
</div> 


	</div>
	
</body>
<script type="text/javascript" src="/static/layui/layui.js"></script>
<script type="text/javascript">layui.config({base: '/static/js/'});</script>

<script type="text/javascript">
layui.use(['tool','element','upload'],function(){
	var $ = layui.jquery,layer = layui.layer,form = layui.form,tool = layui.tool,element = layui.element,upload = layui.upload;
	//Hash地址的定位
	var layid = location.hash.replace(/^#test=/, '');
	element.tabChange('test', layid);

	element.on('tab(test)', function(elem){
	location.hash = 'test='+ $(this).attr('lay-id');
	});
	//上传图片-app图
	var uploadInst = upload.render({
		elem: '#invite_app_banner' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,done: function(res){
		  //上传完毕回调
		  if (res.code==1) {
		    	$('#iptiab').val(res.data.id);
		    	$('.appheadimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	var uploadInst2 = upload.render({
		elem: '#invite_wap_banner' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,done: function(res){
		  //上传完毕回调
		  if (res.code==1) {
		    	$('#iptiwb').val(res.data.id);
		    	$('.wapheadimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	var uploadInst3 = upload.render({
		elem: '#invite_share_img' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,done: function(res){
		  //上传完毕回调
		  if (res.code==1) {
		    	$('#iptisi').val(res.data.id);
		    	$('.shareimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	var uploadInst4 = upload.render({
		elem: '#app_share_hb' //绑定元素
		,url: '<?php echo url('Ajax/upload'); ?>' //上传接口
		,exts:'png'//只允许上传png
		,done: function(res){
		  //上传完毕回调
		  if (res.code==1) {
		    	$('#iptash').val(res.data.id);
		    	$('.sharehbimg img').attr('src',res.data.url);
		    }else{
		    	layer.msg(res.data,{icon:2});
		    }
		}
		,error: function(){
		  //请求异常回调
		  layer.msg('上传异常~');
		}
	});
	tool.setValue('config[invite_need_code]','<?php echo htmlentities($config['invite_need_code']); ?>');
	tool.setValue('lvcfg[order_jiesuan_type]','<?php echo htmlentities($lvcfg['order_jiesuan_type']); ?>');
});
</script>

</html>